﻿/*************************************************************************************************
* Page Name  : CapacityController.cs
* Created By : Rakshitha R  
* Created On : 10 Dec 2018
* Description : Capacity report related API's are coded in this controller
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class CapacityModelDTO
    {
        /// <summary>
        /// It stores MID of user
        /// </summary>
        public string EmployeeID { get; set; }

        /// <summary>
        /// It stores the name of user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// It stores the competency of the user
        /// </summary>
        public string Competency { get; set; }

        /// <summary>
        /// It stores the name of project in which user is working
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// It stores the role of the user like Delivery Manager etc
        /// </summary>
        public string GivenRole { get; set; }

        /// <summary>
        /// It stores the location of the user
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// It stores whether employee is working in India(Offshore) or foreign(OnShore)
        /// </summary>
        public string OnOffShore { get; set; }

        /// <summary>
        /// It stores whether employee is Billable or Non-billable
        /// </summary>
        public string BillableStatus { get; set; }

        /// <summary>
        /// It stores the vertical like BIZ-COMM etc
        /// </summary>
        public string Vertical { get; set; }

        /// <summary>
        /// It stores the sub vertical like Banking etc.
        /// </summary>
        public string SubVertical { get; set; }

        /// <summary>
        /// It stores the practise like DIGITAL etc.
        /// </summary>
        public string Practice { get; set; }

        /// <summary>
        /// It stores the sub practise like Mainframe,Datascience etc.
        /// </summary>
        public string SubPractice { get; set; }

        /// <summary>
        /// It stores the profit center like BFSI etc.
        /// </summary>
        public string ProfitCenter { get; set; }

        /// <summary>
        /// It stores the account name
        /// </summary>
        public string AccountGroup { get; set; }

        /// <summary>
        /// It stores the gender as Male or Female
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// it stores the delivery manager
        /// </summary>
        public string DeliveryManager { get; set; }

        /// <summary>
        /// It stores the engagement type
        /// </summary>
        public string EngagementType { get; set; }

        /// <summary>
        /// It stores the name of account
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// It stores the status of SR
        /// </summary>
        public string SrStatus { get; set; }

        /// <summary>
        /// It stores the Delivery partner
        /// </summary>
        public string DeliveryPartner { get; set; }
        /// <summary>
        /// It stores the Title
        /// </summary>
        public string Title { get; set; }
    }
}
