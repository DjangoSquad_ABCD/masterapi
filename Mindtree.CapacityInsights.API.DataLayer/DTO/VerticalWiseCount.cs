﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class VerticalWiseCount
    {
        /// <summary>
        /// Store the vertical name
        /// </summary>
        public string Vertical { get; set; }

        /// <summary>
        /// store the count of the people in the respective vertical
        /// </summary>
        public int VerticalCount { get; set; }
    }
}
