﻿/*************************************************************************************************
* Page Name  : PreferencesModelDTO.cs
* Created By :   
* Created On : 
* Description : Properties for saving filter panel details
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{

    /// <summary>
    /// To store preference for the specific MID 
    /// <returns></returns>
    public class PreferencesModelDTO
    {
        /// <summary>
        /// It stores the MID of user
        /// </summary>
        public string Mid { get; set; }
        /// <summary>
        /// It stores the default preference of user
        /// </summary>
        public bool DefaultPreference { get; set; }
        /// <summary>
        /// It stores the name of filter created by user(anything of user's choice)
        /// </summary>
        public string FilterName { get; set; }
        /// <summary>
        /// It stores preferences of user
        /// </summary>
        public FilterModelDTO Preferences { get; set; }
        /// <summary>
        /// It stores the date of saving filter
        /// </summary>
        public DateTime UploadedDate { get; set; }
        /// <summary>
        /// It stores the name of user who saved it
        /// </summary>
        public string Uploadedby { get; set; }
    }
}
