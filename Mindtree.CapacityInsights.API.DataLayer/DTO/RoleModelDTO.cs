﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{ 
    /// <summary>
  /// To store role for the specific MID 
  /// <returns></returns>
    public class RoleModelDTO
    {
        /// <summary>
        /// It stores the MID of person who wants to be admin
        /// </summary>
        public string MID { get; set; }
        /// <summary>
        /// It stores the role of the user
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// It stores the MID of person who is logged in
        /// </summary>
        public string MidOfLoggedInUser { get; set; }
    }
}
