﻿/***************************************************************************************************************************
* Page Name  : FilterModelDTO.cs
* Created By : Rakshitha R  
* Created On : 1 Dec 2018
* Description : DTO of Filter Type
* Modified By  :  Rakshitha R
* Modified Date :  18 Jan 2019
* Modified Reason: Added variables
***************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
  public  class FilterModelDTO
    {
        public string[] DeliveryPartner { get; set; }
        public string[] BillableStatus { get; set; }
        public string[] OnOffshore { get; set; }
        public string[] Gender { get; set; }
        public string[] SRStatus { get; set; }
        public string[] ProfitCenter { get; set; }
        public string[] Location { get; set; }
        public string[] AccountGroup { get; set; }
        public string[] DeliveryManager { get; set; }
        public string[] CompetencyCode { get; set; }
        public string[] EngagementCode { get; set; }
        public string[] ProjectName { get; set; }
        public string[] Practice { get; set; }
    }
}
