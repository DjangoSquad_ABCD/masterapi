﻿/*************************************************************************************************
* Page Name  : DeliveryManagerCount.cs
* Created By : Supraja Harish
* Created On : 
* Description : Properties for saving Delivery manager count in Dashboard
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class DeliveryManagerCount
    {
        /// <summary>
        /// It stores the name of Delivery Manager
        /// </summary>
        public string DeliveryManger { get; set; }
        /// <summary>
        /// It stores the count of Delivery manager
        /// </summary>
        public int DeliveryMangerCount { get; set; }
    }
}
