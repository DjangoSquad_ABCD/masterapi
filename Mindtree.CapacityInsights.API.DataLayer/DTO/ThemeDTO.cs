﻿/*************************************************************************************************
* Page Name  : ThemeDTO.cs
* Created By : Supraja Harish
* Created On : 
* Description : Theme DTO class
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class ThemeDTO
    {
        /// <summary>
        /// It stores the MID of employee
        /// </summary>
        public string MID { get; set; }
        /// <summary>
        /// It stores the theme name
        /// </summary>
        public string Theme { get; set; }
    }
}
