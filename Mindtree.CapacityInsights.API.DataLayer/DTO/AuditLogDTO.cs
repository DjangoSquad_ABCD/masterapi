﻿/*************************************************************************************************
* Page Name  : AuditLogDTO.cs
* Created By : Supraja Harish
* Created On : 
* Description : Properties for saving in Audit table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class AuditLogDTO
    {
        /// <summary>
        /// It stores the MID of the logged in user
        /// </summary>
        public string EmployeeID { get; set; }
        /// <summary>
        /// It stores the name of the employee of logged in user
        /// </summary>
        public string EmployeeName { get; set; }
        /// <summary>
        /// It stores the action of user like LoggedIn or not
        /// </summary>
        public string UserAction { get; set; }
        /// <summary>
        /// It stores the additional information of user like loggedIn from Desktop etc.
        /// </summary>
        public string Reference { get; set; }
    }
}
