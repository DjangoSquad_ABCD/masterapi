﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.DataLayer.DTO
{
    public class UserModelDTO
    {
        /// <summary>
        /// It stores the MID of employee
        /// </summary>
        public string EmployeeID { get; set; }
        /// <summary>
        /// It stores the name of employee
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// It stores the competency of employee
        /// </summary>
        public string Competency { get; set; }
        /// <summary>
        /// It stores the token for authorization
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// It stores the role of logged in employee
        /// </summary>
        public int Role { get; set; }
    }
}
