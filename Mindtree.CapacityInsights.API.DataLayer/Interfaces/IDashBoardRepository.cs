﻿/***************************************************************************************************************************
* Page Name  : IDashBoardRepository.cs
* Created By : Madhushree Sv 
* Created On : 28 Nov 2018
* Description : Dashboard Repository interface
* Modified By  :  
* Modified Date :  
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.DataLayer.Interfaces
{
    public interface IDashBoardRepository
    {
        /// <summary>
        /// This is controller get method to get Mindtree minds count
        /// </summary>
        /// <returns>Mindtree minds count</returns
        Task<int> GetMindtreemindsCount();
        /// <summary>
        /// This is controller get method to get Competency building count
        /// </summary>
        /// <returns>Competency building</returns>
        Task<int> GetCompetencyBuildingCount();
        /// <summary>
        /// This is controller get method to get deployable count
        /// </summary>
        /// <returns>deployable count</returns>
        Task<int> GetDeployableCount();
        /// <summary>
        /// This is controller post method to get UnBilled minds count
        /// </summary>
        /// <returns>Unbilled minds count</returns>
        Task<int> GetUnBilledMindsCount();
        /// <summary>
        /// This is controller post method to get Billed minds count
        /// </summary>
        /// <returns>Billed minds count</returns>
        Task<int> GetBilledMindsCount();

        /// <summary>
        /// Get onsite offshore count
        /// </summary>
        /// <returns></returns>
        Task<List<int>> GetOnOffShoreCount();
        /// <summary>
        /// Get male female count
        /// </summary>
        /// <returns></returns>
        Task<List<int>> GetMaleFemaleCount();
        /// <summary>
        /// Get engagement type count
        /// </summary>
        /// <returns></returns>
        Task<List<int>> GetEngagementTypeCount();

        /// <summary>
        /// Get number of people under a delivery manager
        /// </summary>
        /// <returns></returns>
        Task<List<DeliveryManagerCount>> GetDeliveryManager();

        /// <summary>
        /// Get number of the people vertical wise
        /// </summary>
        /// <returns></returns>
        Task<List<VerticalWiseCount>> GetVerticalCount();
    }
}
