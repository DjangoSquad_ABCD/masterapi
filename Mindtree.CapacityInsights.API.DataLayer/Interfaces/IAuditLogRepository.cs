﻿using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.DataLayer.Interfaces
{
    public interface IAuditLogRepository
    {
        /// <summary>
        /// Save user footprints
        /// </summary>
        /// <param name="auditLogDTO"></param>
        /// <returns></returns>
        Task SaveLoginHistory(AuditLogDTO auditLogDTO);
    }
}
