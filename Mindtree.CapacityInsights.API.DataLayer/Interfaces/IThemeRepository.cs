﻿/*************************************************************************************************
* Page Name  : IThemeRepository.cs
* Created By : Supraja Harish
* Created On : 
* Description : IThemeRepository interface
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.DataLayer.Interfaces
{
    public interface IThemeRepository
    {
        /// <summary>
        /// Save theme selected by the user
        /// </summary>
        /// <param name="themeDTO">theme object DTO</param>
        /// <returns></returns>
        Task<Boolean> SaveTheme(ThemeDTO themeDTO);

        /// <summary>
        /// Get theme saved by the user
        /// </summary>
        /// <param name="MID">MID of current user</param>
        /// <returns></returns>
        Task<ThemeDTO> GetTheme(string MID);
    }
}
