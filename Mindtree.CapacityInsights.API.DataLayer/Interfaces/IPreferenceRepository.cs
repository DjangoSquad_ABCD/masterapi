﻿/***************************************************************************************************************************
* Page Name  : IPreferenceRepository.cs
* Created By :  
* Created On : 
* Description : Preference related API's DAL layer interface
* Modified By  : Supraja Harish   
* Modified Date :  02/07/2019  
* Modified Reason: Code comments
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.DataLayer.Interfaces
{
     public interface IPreferenceRepository
    {
        /// <summary>
        /// Method to save preferences in db
        /// </summary>
        /// <param name="preferencesModelDTO">Preference model DTO</param>
        /// <returns></returns>
        Task<bool> SavePreferences(PreferencesModelDTO preferencesModelDTO);

        /// <summary>
        /// To get all preference data from db
        /// </summary>
        /// <param name="MID">MID of the current user.</param>
        /// <returns></returns>
        Task<List<PreferencesModelDTO>> GetTblProfilePreference(string MID);


        /// <summary>
        /// To get default preference if any 
        /// </summary>
        /// <param name="MID">MID of the current user.</param>
        /// <returns></returns>
        Task<PreferencesModelDTO> GetDefaultPreference(string MID);

        /// <summary>
        /// Get defalut prefence name to display
        /// </summary>
        /// <param name="MID">MID of the current user.</param>
        /// <returns></returns>
        Task<PreferencesModelDTO> GetDefaultPreferenceForDisplay(string MID);

        /// <summary>
        /// To update default preference
        /// </summary>
        /// <param name="preference">Preference model DTO</param>
        /// <returns></returns>
        Task UpdateDefaultPreference(PreferencesModelDTO  preference);
    }

}
