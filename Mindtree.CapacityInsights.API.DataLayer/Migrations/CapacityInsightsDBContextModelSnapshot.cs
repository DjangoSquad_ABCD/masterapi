﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Mindtree.CapacityInsights.API.DataLayer;

namespace Mindtree.CapacityInsights.API.DataLayer.Migrations
{
    [DbContext(typeof(CapacityInsightsDBContext))]
    partial class CapacityInsightsDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblAuditLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("EmployeeId")
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("EmployeeName")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                    b.Property<DateTime?>("LastUpdatedOn")
                        .HasColumnType("datetime");

                    b.Property<string>("Reference")
                        .IsUnicode(false);

                    b.Property<string>("ReqId")
                        .HasColumnName("ReqID")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("UploadedBy")
                        .HasMaxLength(32);

                    b.Property<DateTime?>("UploadedDateTime")
                        .HasColumnType("datetime");

                    b.Property<string>("UploadedFor")
                        .HasMaxLength(32);

                    b.Property<string>("UserAction")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.ToTable("tblAuditLog");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblCapacityReport", b =>
                {
                    b.Property<string>("EmployeeId")
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(50);

                    b.Property<string>("AccountCategory");

                    b.Property<string>("AccountGroup");

                    b.Property<int?>("AvailableF");

                    b.Property<string>("Billability");

                    b.Property<double?>("BillablePercentageStaffed");

                    b.Property<string>("BillableStatus");

                    b.Property<double?>("BilledF");

                    b.Property<int?>("BlockedF");

                    b.Property<string>("CampusTag");

                    b.Property<string>("Comments");

                    b.Property<string>("Competency");

                    b.Property<string>("ConfirmedOn")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("CurrentWorkLocation");

                    b.Property<string>("CustomerGroup");

                    b.Property<string>("CustomerName");

                    b.Property<string>("DateOfJoining");

                    b.Property<string>("DeliveryManager");

                    b.Property<string>("DeliveryPartner");

                    b.Property<string>("EmpStatus");

                    b.Property<string>("EndDate");

                    b.Property<string>("EngagementType");

                    b.Property<string>("ExpiryDate");

                    b.Property<string>("GivenFunction");

                    b.Property<string>("GivenRole");

                    b.Property<string>("GivenType");

                    b.Property<string>("IgdeliveryHead")
                        .HasColumnName("IGDeliveryHead");

                    b.Property<string>("IjpappliedDate")
                        .HasColumnName("IJPAppliedDate");

                    b.Property<string>("IjpappliedSrid")
                        .HasColumnName("IJPAppliedSRID");

                    b.Property<string>("Ijpstuatus")
                        .HasColumnName("IJPStuatus");

                    b.Property<int?>("InternalPrjCostF");

                    b.Property<int?>("InternalPrjInvestmentF");

                    b.Property<bool?>("IsDataCorrect")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("((1))");

                    b.Property<string>("Klocation")
                        .HasColumnName("KLocation");

                    b.Property<string>("LastStaffingEndDate");

                    b.Property<string>("LastUpdatedBy")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("LastUpdatedOn")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Location");

                    b.Property<int?>("LongLeaveF");

                    b.Property<string>("Name");

                    b.Property<int?>("NonDeployF");

                    b.Property<string>("OnOffShore");

                    b.Property<string>("PeopleOwner");

                    b.Property<string>("Practice");

                    b.Property<string>("PreferredWorkLocation");

                    b.Property<string>("PrimaryL1");

                    b.Property<string>("PrimaryL2");

                    b.Property<string>("PrimaryL3");

                    b.Property<string>("PrimaryL4");

                    b.Property<string>("ProfitCenter");

                    b.Property<string>("ProjectId")
                        .HasColumnName("ProjectID");

                    b.Property<string>("ProjectManagerRequestor");

                    b.Property<string>("ProjectName");

                    b.Property<string>("ProjectRole");

                    b.Property<string>("ProjectType");

                    b.Property<string>("ReasonForBlocking");

                    b.Property<string>("ReasonForUnbillable");

                    b.Property<string>("Remarks");

                    b.Property<string>("RequiredYear");

                    b.Property<string>("Resigned");

                    b.Property<string>("Rtpf")
                        .HasColumnName("RTPF");

                    b.Property<int?>("SalesF");

                    b.Property<string>("SecondaryL1");

                    b.Property<string>("SecondaryL2");

                    b.Property<string>("SecondaryL3");

                    b.Property<string>("SecondaryL4");

                    b.Property<string>("Segment");

                    b.Property<string>("Srstatus")
                        .HasColumnName("SRStatus");

                    b.Property<string>("StaffingRequestId")
                        .HasColumnName("StaffingRequestID");

                    b.Property<string>("StampingStatus");

                    b.Property<string>("StartDate");

                    b.Property<string>("SubCategory");

                    b.Property<string>("SubPractice");

                    b.Property<string>("SubVertical");

                    b.Property<int?>("SupportF");

                    b.Property<string>("Technology");

                    b.Property<string>("TertiaryL1");

                    b.Property<string>("TertiaryL2");

                    b.Property<string>("TertiaryL3");

                    b.Property<string>("TertiaryL4");

                    b.Property<string>("Title");

                    b.Property<string>("TotalBlockedDays");

                    b.Property<int?>("TotalExperience");

                    b.Property<double?>("TotalPercentageStaffed");

                    b.Property<int?>("TrainingF");

                    b.Property<string>("UnbillableDays");

                    b.Property<double?>("UnbillablePercentageStaffed");

                    b.Property<int?>("UnbilledF");

                    b.Property<string>("Vertical");

                    b.Property<string>("VisaCategory");

                    b.Property<string>("WorkLocation");

                    b.Property<string>("Yorbit101");

                    b.Property<string>("Yorbit201");

                    b.Property<string>("Yorbit301");

                    b.HasKey("EmployeeId");

                    b.ToTable("tblCapacityReport");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblCapacityReportStaging", b =>
                {
                    b.Property<int>("RowId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("RowID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AccountCategory");

                    b.Property<string>("AccountGroup");

                    b.Property<string>("AvailableF");

                    b.Property<string>("Billability");

                    b.Property<string>("BillablePercentageStaffed");

                    b.Property<string>("BillableStatus");

                    b.Property<string>("BilledF");

                    b.Property<string>("BlockedF");

                    b.Property<string>("CampusTag");

                    b.Property<string>("Comments");

                    b.Property<string>("Competency");

                    b.Property<string>("CurrentWorkLocation");

                    b.Property<string>("CustomerGroup");

                    b.Property<string>("CustomerName");

                    b.Property<string>("DateOfJoining");

                    b.Property<string>("DeliveryManager");

                    b.Property<string>("DeliveryPartner");

                    b.Property<string>("EmpStatus");

                    b.Property<string>("EmployeeId")
                        .IsRequired()
                        .HasColumnName("EmployeeID")
                        .HasMaxLength(50);

                    b.Property<string>("EndDate");

                    b.Property<string>("EngagementType");

                    b.Property<string>("ExpiryDate");

                    b.Property<string>("GivenFunction");

                    b.Property<string>("GivenRole");

                    b.Property<string>("GivenType");

                    b.Property<string>("IgdeliveryHead")
                        .HasColumnName("IGDeliveryHead");

                    b.Property<string>("IjpappliedDate")
                        .HasColumnName("IJPAppliedDate");

                    b.Property<string>("IjpappliedSrid")
                        .HasColumnName("IJPAppliedSRID");

                    b.Property<string>("Ijpstuatus")
                        .HasColumnName("IJPStuatus");

                    b.Property<string>("InternalPrjCostF");

                    b.Property<string>("InternalPrjInvestmentF");

                    b.Property<string>("Klocation")
                        .HasColumnName("KLocation");

                    b.Property<string>("LastStaffingEndDate");

                    b.Property<string>("LastUpdatedBy")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("LastUpdatedOn")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Location");

                    b.Property<string>("LongLeaveF");

                    b.Property<string>("Name");

                    b.Property<string>("NonDeployF");

                    b.Property<string>("OnOffShore");

                    b.Property<string>("PeopleOwner");

                    b.Property<string>("Practice");

                    b.Property<string>("PreferredWorkLocation");

                    b.Property<string>("PrimaryL1");

                    b.Property<string>("PrimaryL2");

                    b.Property<string>("PrimaryL3");

                    b.Property<string>("PrimaryL4");

                    b.Property<string>("ProfitCenter");

                    b.Property<string>("ProjectId")
                        .HasColumnName("ProjectID");

                    b.Property<string>("ProjectManagerRequestor");

                    b.Property<string>("ProjectName");

                    b.Property<string>("ProjectRole");

                    b.Property<string>("ProjectType");

                    b.Property<string>("ReasonForBlocking");

                    b.Property<string>("ReasonForUnbillable");

                    b.Property<string>("Remarks");

                    b.Property<string>("RequiredYear");

                    b.Property<string>("Resigned");

                    b.Property<string>("Rtpf")
                        .HasColumnName("RTPF");

                    b.Property<string>("SalesF");

                    b.Property<string>("SecondaryL1");

                    b.Property<string>("SecondaryL2");

                    b.Property<string>("SecondaryL3");

                    b.Property<string>("SecondaryL4");

                    b.Property<string>("Segment");

                    b.Property<string>("Srstatus")
                        .HasColumnName("SRStatus");

                    b.Property<string>("StaffingRequestId")
                        .HasColumnName("StaffingRequestID");

                    b.Property<string>("StampingStatus");

                    b.Property<string>("StartDate");

                    b.Property<string>("SubCategory");

                    b.Property<string>("SubPractice");

                    b.Property<string>("SubVertical");

                    b.Property<string>("SupportF");

                    b.Property<string>("Technology");

                    b.Property<string>("TertiaryL1");

                    b.Property<string>("TertiaryL2");

                    b.Property<string>("TertiaryL3");

                    b.Property<string>("TertiaryL4");

                    b.Property<string>("Title");

                    b.Property<string>("TotalBlockedDays");

                    b.Property<int?>("TotalExperience");

                    b.Property<string>("TotalPercentageStaffed");

                    b.Property<string>("TrainingF");

                    b.Property<string>("UnbillableDays");

                    b.Property<string>("UnbillablePercentageStaffed");

                    b.Property<string>("UnbilledF");

                    b.Property<string>("Vertical");

                    b.Property<string>("VisaCategory");

                    b.Property<string>("WorkLocation");

                    b.Property<string>("Yorbit101");

                    b.Property<string>("Yorbit201");

                    b.Property<string>("Yorbit301");

                    b.HasKey("RowId");

                    b.ToTable("tblCapacityReport_Staging");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblExceptionLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<DateTime?>("CreatedOn")
                        .HasColumnType("datetime");

                    b.Property<string>("ExceptionLevel")
                        .HasMaxLength(10)
                        .IsUnicode(false);

                    b.Property<string>("Message")
                        .IsUnicode(false);

                    b.Property<string>("Source")
                        .IsUnicode(false);

                    b.Property<string>("StackTrace")
                        .HasColumnType("text");

                    b.Property<string>("TargetSite")
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.ToTable("tblExceptionLog");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblProfilePrefrence", b =>
                {
                    b.Property<string>("Mid")
                        .HasColumnName("MID")
                        .HasMaxLength(50);

                    b.Property<string>("FilterName")
                        .HasColumnName("FilterName")
                        .HasMaxLength(400);

                    b.Property<bool>("DefaultPreference");

                    b.Property<string>("Preferences");

                    b.Property<DateTime>("UploadedDate")
                        .HasColumnType("datetime");

                    b.Property<string>("Uploadedby")
                        .HasColumnName("Uploadedby")
                        .HasMaxLength(50);

                    b.HasKey("Mid", "FilterName");

                    b.ToTable("tblProfilePrefrence");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblRole", b =>
                {
                    b.Property<int>("RId")
                        .HasColumnName("RID")
                        .HasMaxLength(10);

                    b.Property<string>("GivenRole");

                    b.Property<string>("RoleDescription");

                    b.HasKey("RId");

                    b.ToTable("tblRole");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblSaveRole", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnName("Id")
                        .HasMaxLength(10);

                    b.Property<int>("ActiveInactive");

                    b.Property<string>("MID");

                    b.Property<int>("RID");

                    b.Property<DateTime>("UploadedDate");

                    b.Property<string>("Uploadedby");

                    b.HasKey("Id");

                    b.ToTable("tblSaveRole");
                });

            modelBuilder.Entity("Mindtree.CapacityInsights.API.Entities.DataEntities.TblSaveTheme", b =>
                {
                    b.Property<string>("MID")
                        .HasColumnName("MID")
                        .HasMaxLength(50);

                    b.Property<string>("Theme")
                        .IsRequired()
                        .HasColumnName("Theme")
                        .HasMaxLength(400);

                    b.HasKey("MID");

                    b.ToTable("tblSaveTheme");
                });
#pragma warning restore 612, 618
        }
    }
}
