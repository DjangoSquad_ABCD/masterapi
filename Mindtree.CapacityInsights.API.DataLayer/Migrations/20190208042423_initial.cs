﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mindtree.CapacityInsights.API.DataLayer.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblAuditLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    EmployeeName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    UserAction = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ReqID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Reference = table.Column<string>(unicode: false, nullable: true),
                    LastUpdatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    UploadedDateTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    UploadedBy = table.Column<string>(maxLength: 32, nullable: true),
                    UploadedFor = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblAuditLog", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "tblCapacityReport",
                columns: table => new
                {
                    EmployeeID = table.Column<string>(maxLength: 50, nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Competency = table.Column<string>(nullable: true),
                    GivenRole = table.Column<string>(nullable: true),
                    Segment = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    OnOffShore = table.Column<string>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    Billability = table.Column<string>(nullable: true),
                    Vertical = table.Column<string>(nullable: true),
                    SubVertical = table.Column<string>(nullable: true),
                    Practice = table.Column<string>(nullable: true),
                    SubPractice = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    ProfitCenter = table.Column<string>(nullable: true),
                    ProjectID = table.Column<string>(nullable: true),
                    ProjectName = table.Column<string>(nullable: true),
                    EngagementType = table.Column<string>(nullable: true),
                    ProjectType = table.Column<string>(nullable: true),
                    ProjectRole = table.Column<string>(nullable: true),
                    ProjectManagerRequestor = table.Column<string>(nullable: true),
                    DeliveryManager = table.Column<string>(nullable: true),
                    DeliveryPartner = table.Column<string>(nullable: true),
                    IGDeliveryHead = table.Column<string>(nullable: true),
                    BillableStatus = table.Column<string>(nullable: true),
                    StartDate = table.Column<string>(nullable: true),
                    EndDate = table.Column<string>(nullable: true),
                    SRStatus = table.Column<string>(nullable: true),
                    StaffingRequestID = table.Column<string>(nullable: true),
                    ReasonForBlocking = table.Column<string>(nullable: true),
                    ReasonForUnbillable = table.Column<string>(nullable: true),
                    UnbillableDays = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    CampusTag = table.Column<string>(nullable: true),
                    BillablePercentageStaffed = table.Column<double>(nullable: true),
                    UnbillablePercentageStaffed = table.Column<double>(nullable: true),
                    TotalPercentageStaffed = table.Column<double>(nullable: true),
                    Technology = table.Column<string>(nullable: true),
                    DateOfJoining = table.Column<string>(nullable: true),
                    GivenFunction = table.Column<string>(nullable: true),
                    AccountGroup = table.Column<string>(nullable: true),
                    VisaCategory = table.Column<string>(nullable: true),
                    SubCategory = table.Column<string>(nullable: true),
                    GivenType = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<string>(nullable: true),
                    StampingStatus = table.Column<string>(nullable: true),
                    Comments = table.Column<string>(nullable: true),
                    RequiredYear = table.Column<string>(nullable: true),
                    IJPStuatus = table.Column<string>(nullable: true),
                    IJPAppliedSRID = table.Column<string>(nullable: true),
                    IJPAppliedDate = table.Column<string>(nullable: true),
                    WorkLocation = table.Column<string>(nullable: true),
                    KLocation = table.Column<string>(nullable: true),
                    CustomerGroup = table.Column<string>(nullable: true),
                    BilledF = table.Column<double>(nullable: true),
                    UnbilledF = table.Column<int>(nullable: true),
                    BlockedF = table.Column<int>(nullable: true),
                    InternalPrjInvestmentF = table.Column<int>(nullable: true),
                    InternalPrjCostF = table.Column<int>(nullable: true),
                    AvailableF = table.Column<int>(nullable: true),
                    NonDeployF = table.Column<int>(nullable: true),
                    TrainingF = table.Column<int>(nullable: true),
                    LongLeaveF = table.Column<int>(nullable: true),
                    SalesF = table.Column<int>(nullable: true),
                    SupportF = table.Column<int>(nullable: true),
                    PeopleOwner = table.Column<string>(nullable: true),
                    AccountCategory = table.Column<string>(nullable: true),
                    PrimaryL1 = table.Column<string>(nullable: true),
                    PrimaryL2 = table.Column<string>(nullable: true),
                    PrimaryL3 = table.Column<string>(nullable: true),
                    PrimaryL4 = table.Column<string>(nullable: true),
                    SecondaryL1 = table.Column<string>(nullable: true),
                    SecondaryL2 = table.Column<string>(nullable: true),
                    SecondaryL3 = table.Column<string>(nullable: true),
                    SecondaryL4 = table.Column<string>(nullable: true),
                    TertiaryL1 = table.Column<string>(nullable: true),
                    TertiaryL2 = table.Column<string>(nullable: true),
                    TertiaryL3 = table.Column<string>(nullable: true),
                    TertiaryL4 = table.Column<string>(nullable: true),
                    RTPF = table.Column<string>(nullable: true),
                    Resigned = table.Column<string>(nullable: true),
                    CurrentWorkLocation = table.Column<string>(nullable: true),
                    PreferredWorkLocation = table.Column<string>(nullable: true),
                    LastStaffingEndDate = table.Column<string>(nullable: true),
                    LastUpdatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LastUpdatedOn = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    IsDataCorrect = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    ConfirmedOn = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Yorbit101 = table.Column<string>(nullable: true),
                    Yorbit201 = table.Column<string>(nullable: true),
                    Yorbit301 = table.Column<string>(nullable: true),
                    TotalBlockedDays = table.Column<string>(nullable: true),
                    TotalExperience = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCapacityReport", x => x.EmployeeID);
                });

            migrationBuilder.CreateTable(
                name: "tblCapacityReport_Staging",
                columns: table => new
                {
                    RowID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeID = table.Column<string>(maxLength: 50, nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Competency = table.Column<string>(nullable: true),
                    GivenRole = table.Column<string>(nullable: true),
                    Segment = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    OnOffShore = table.Column<string>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    Billability = table.Column<string>(nullable: true),
                    Vertical = table.Column<string>(nullable: true),
                    SubVertical = table.Column<string>(nullable: true),
                    Practice = table.Column<string>(nullable: true),
                    SubPractice = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    ProfitCenter = table.Column<string>(nullable: true),
                    ProjectID = table.Column<string>(nullable: true),
                    ProjectName = table.Column<string>(nullable: true),
                    EngagementType = table.Column<string>(nullable: true),
                    ProjectType = table.Column<string>(nullable: true),
                    ProjectRole = table.Column<string>(nullable: true),
                    ProjectManagerRequestor = table.Column<string>(nullable: true),
                    DeliveryManager = table.Column<string>(nullable: true),
                    DeliveryPartner = table.Column<string>(nullable: true),
                    IGDeliveryHead = table.Column<string>(nullable: true),
                    BillableStatus = table.Column<string>(nullable: true),
                    StartDate = table.Column<string>(nullable: true),
                    EndDate = table.Column<string>(nullable: true),
                    SRStatus = table.Column<string>(nullable: true),
                    StaffingRequestID = table.Column<string>(nullable: true),
                    ReasonForBlocking = table.Column<string>(nullable: true),
                    ReasonForUnbillable = table.Column<string>(nullable: true),
                    UnbillableDays = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    CampusTag = table.Column<string>(nullable: true),
                    BillablePercentageStaffed = table.Column<string>(nullable: true),
                    UnbillablePercentageStaffed = table.Column<string>(nullable: true),
                    TotalPercentageStaffed = table.Column<string>(nullable: true),
                    Technology = table.Column<string>(nullable: true),
                    DateOfJoining = table.Column<string>(nullable: true),
                    GivenFunction = table.Column<string>(nullable: true),
                    AccountGroup = table.Column<string>(nullable: true),
                    TotalBlockedDays = table.Column<string>(nullable: true),
                    VisaCategory = table.Column<string>(nullable: true),
                    SubCategory = table.Column<string>(nullable: true),
                    GivenType = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<string>(nullable: true),
                    StampingStatus = table.Column<string>(nullable: true),
                    Comments = table.Column<string>(nullable: true),
                    RequiredYear = table.Column<string>(nullable: true),
                    IJPStuatus = table.Column<string>(nullable: true),
                    IJPAppliedSRID = table.Column<string>(nullable: true),
                    IJPAppliedDate = table.Column<string>(nullable: true),
                    WorkLocation = table.Column<string>(nullable: true),
                    KLocation = table.Column<string>(nullable: true),
                    CustomerGroup = table.Column<string>(nullable: true),
                    BilledF = table.Column<string>(nullable: true),
                    UnbilledF = table.Column<string>(nullable: true),
                    BlockedF = table.Column<string>(nullable: true),
                    InternalPrjInvestmentF = table.Column<string>(nullable: true),
                    InternalPrjCostF = table.Column<string>(nullable: true),
                    AvailableF = table.Column<string>(nullable: true),
                    NonDeployF = table.Column<string>(nullable: true),
                    TrainingF = table.Column<string>(nullable: true),
                    LongLeaveF = table.Column<string>(nullable: true),
                    SalesF = table.Column<string>(nullable: true),
                    SupportF = table.Column<string>(nullable: true),
                    AccountCategory = table.Column<string>(nullable: true),
                    PeopleOwner = table.Column<string>(nullable: true),
                    PrimaryL1 = table.Column<string>(nullable: true),
                    PrimaryL2 = table.Column<string>(nullable: true),
                    PrimaryL3 = table.Column<string>(nullable: true),
                    PrimaryL4 = table.Column<string>(nullable: true),
                    SecondaryL1 = table.Column<string>(nullable: true),
                    SecondaryL2 = table.Column<string>(nullable: true),
                    SecondaryL3 = table.Column<string>(nullable: true),
                    SecondaryL4 = table.Column<string>(nullable: true),
                    TertiaryL1 = table.Column<string>(nullable: true),
                    TertiaryL2 = table.Column<string>(nullable: true),
                    TertiaryL3 = table.Column<string>(nullable: true),
                    TertiaryL4 = table.Column<string>(nullable: true),
                    RTPF = table.Column<string>(nullable: true),
                    Resigned = table.Column<string>(nullable: true),
                    CurrentWorkLocation = table.Column<string>(nullable: true),
                    PreferredWorkLocation = table.Column<string>(nullable: true),
                    Yorbit101 = table.Column<string>(nullable: true),
                    Yorbit201 = table.Column<string>(nullable: true),
                    Yorbit301 = table.Column<string>(nullable: true),
                    LastStaffingEndDate = table.Column<string>(nullable: true),
                    LastUpdatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    LastUpdatedOn = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TotalExperience = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCapacityReport_Staging", x => x.RowID);
                });

            migrationBuilder.CreateTable(
                name: "tblExceptionLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExceptionLevel = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Source = table.Column<string>(unicode: false, nullable: true),
                    Message = table.Column<string>(unicode: false, nullable: true),
                    StackTrace = table.Column<string>(type: "text", nullable: true),
                    TargetSite = table.Column<string>(unicode: false, nullable: true),
                    CreatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblExceptionLog", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "tblProfilePrefrence",
                columns: table => new
                {
                    MID = table.Column<string>(maxLength: 50, nullable: false),
                    DefaultPreference = table.Column<bool>(nullable: false),
                    FilterName = table.Column<string>(maxLength: 400, nullable: false),
                    Preferences = table.Column<string>(nullable: true),
                    UploadedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Uploadedby = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblProfilePrefrence", x => new { x.MID, x.FilterName });
                });

            migrationBuilder.CreateTable(
                name: "tblRole",
                columns: table => new
                {
                    RID = table.Column<int>(maxLength: 10, nullable: false),
                    GivenRole = table.Column<string>(nullable: true),
                    RoleDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblRole", x => x.RID);
                });

            migrationBuilder.CreateTable(
                name: "tblSaveRole",
                columns: table => new
                {
                    Id = table.Column<Guid>(maxLength: 10, nullable: false),
                    MID = table.Column<string>(nullable: true),
                    RID = table.Column<int>(nullable: false),
                    ActiveInactive = table.Column<int>(nullable: false),
                    UploadedDate = table.Column<DateTime>(nullable: false),
                    Uploadedby = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSaveRole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblSaveTheme",
                columns: table => new
                {
                    MID = table.Column<string>(maxLength: 50, nullable: false),
                    Theme = table.Column<string>(maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSaveTheme", x => x.MID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblAuditLog");

            migrationBuilder.DropTable(
                name: "tblCapacityReport");

            migrationBuilder.DropTable(
                name: "tblCapacityReport_Staging");

            migrationBuilder.DropTable(
                name: "tblExceptionLog");

            migrationBuilder.DropTable(
                name: "tblProfilePrefrence");

            migrationBuilder.DropTable(
                name: "tblRole");

            migrationBuilder.DropTable(
                name: "tblSaveRole");

            migrationBuilder.DropTable(
                name: "tblSaveTheme");
        }
    }
}
