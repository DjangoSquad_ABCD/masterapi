﻿/***************************************************************************************************************************
* Page Name  : CapacityController.cs
* Created By : Rakshitha R  
* Created On : 10 Dec 2018
* Description : Capacity report related API's are coded in this controller
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Dapper;
namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class CapacityRepository : ICapacityRepository
    {
        #region private variables
        private readonly CapacityInsightsDBContext _context;
        private readonly IHostingEnvironment _env;
        #endregion

        #region constructor
        public CapacityRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }

        #endregion

        #region GetCapacityData
        /// <summary>
        /// To get capacity data from db based on user prefrence or default
        /// </summary>
        /// <returns>List of capacity data</returns>
        public async Task<List<CapacityModelDTO>> GetCapacityData(string MID)
        {
            try
            {

                //check if the user has a prefrence set by default
                string preference = await _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID) && a.DefaultPreference == true).Select(a => a.Preferences).FirstOrDefaultAsync();
                if (preference == null)
                {
                    preference = await _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID)).Select(a => a.Preferences).FirstOrDefaultAsync();
                }

                //If preference is set then filter data based on the preference filter.
                if (preference != null)
                {
                    FilterModelDTO defaultPreference = JsonConvert.DeserializeObject<FilterModelDTO>(preference);
                    return await GetFilteredCapacityData(defaultPreference);
                }

                //else show first 100 records by default
                else
                {
                    var capacitydata = GetCapacity(MID);
                    return capacitydata;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion


        #region GetCapacity
        public List<CapacityModelDTO> GetCapacity(string MID)
        {
            try
            {
                return _context.TblCapacityReportStaging.Select(p => new CapacityModelDTO
                {
                    EmployeeID = p.EmployeeId.ToUpper(),
                    Name = p.Name.ToUpper(),
                    Competency = p.Competency.ToUpper(),
                    ProjectName = p.ProjectName.ToUpper(),
                    GivenRole = p.GivenRole.ToUpper(),
                    Location = p.Location.ToUpper(),
                    OnOffShore = p.OnOffShore,
                    BillableStatus = p.BillableStatus.ToUpper(),
                    Vertical = p.Vertical.ToUpper(),
                    SubVertical = p.SubVertical.ToUpper(),
                    Practice = p.Practice.ToUpper(),
                    SubPractice = p.SubPractice.ToUpper(),
                    ProfitCenter = p.ProfitCenter.ToUpper(),
                    AccountGroup = p.AccountGroup.ToUpper(),
                    Gender = p.Title.ToUpper(),
                    DeliveryManager = p.DeliveryManager.ToUpper(),
                    EngagementType = p.EngagementType.ToUpper(),
                    AccountName = p.AccountGroup.ToUpper(),
                    DeliveryPartner = p.DeliveryPartner.ToUpper(),
                    SrStatus = p.Srstatus.ToUpper()
                }).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetFilteredCapacityData
        /// <summary>
        /// Fetch capacity data based on the applied filters
        /// </summary>
        /// <param name="filterModelDTO"></param>
        /// <returns></returns>
        public async Task<List<CapacityModelDTO>> GetFilteredCapacityData(FilterModelDTO filterModelDTO)
        {
            try
            {
                string[] DeliveryPartnerList = filterModelDTO.DeliveryPartner;
                string[] BillableStatusList = filterModelDTO.BillableStatus;
                string[] SRStatusList = filterModelDTO.Gender;
                string[] ProfitCenterList = filterModelDTO.ProfitCenter;
                string[] LocationList = filterModelDTO.Location;
                string[] AccountGroupList = filterModelDTO.AccountGroup;
                string[] DeliveryManagerList = filterModelDTO.DeliveryManager;
                string[] EngagementCodeList = filterModelDTO.EngagementCode;
                string[] ProjectNameList = filterModelDTO.ProjectName;
                string[] PracticeList = filterModelDTO.Practice;


                List<CapacityModelDTO> listOfData = await _context.TblCapacityReportStaging.Where(x =>
                                                                         (DeliveryPartnerList.Count() == 0 || (x.DeliveryPartner != null && DeliveryPartnerList.Contains(x.DeliveryPartner))) &&
                                                                         (BillableStatusList.Count() == 0 || (x.BillableStatus != null && BillableStatusList.Contains(x.BillableStatus))) &&
                                                                         (SRStatusList.Count() == 0 || (x.Srstatus != null && SRStatusList.Contains(x.Srstatus))) &&
                                                                         (ProfitCenterList.Count() == 0 || (x.ProfitCenter != null && ProfitCenterList.Contains(x.ProfitCenter))) &&
                                                                         (LocationList.Count() == 0 || (x.Location != null && LocationList.Contains(x.Location))) &&
                                                                         (AccountGroupList.Count() == 0 || (x.AccountGroup != null && AccountGroupList.Contains(x.AccountGroup))) &&
                                                                         (DeliveryManagerList.Count() == 0 || (x.DeliveryManager != null && DeliveryManagerList.Contains(x.DeliveryManager))) &&
                                                                         (EngagementCodeList.Count() == 0 || (x.EngagementType != null && EngagementCodeList.Contains(x.EngagementType))) &&
                                                                         (ProjectNameList.Count() == 0 || (x.ProjectName != null && ProjectNameList.Contains(x.ProjectName))) &&
                                                                         (PracticeList.Count() == 0 || (x.Practice != null && PracticeList.Contains(x.Practice))))
                                                                          .Select(p => new CapacityModelDTO
                                                                          {
                                                                              EmployeeID = p.EmployeeId,
                                                                              Name = p.Name.ToUpper(),
                                                                              Competency = p.Competency.ToUpper(),
                                                                              ProjectName = p.ProjectName.ToUpper(),
                                                                              GivenRole = p.GivenRole.ToUpper(),
                                                                              Location = p.Location.ToUpper(),
                                                                              OnOffShore = p.OnOffShore,
                                                                              BillableStatus = p.BillableStatus.ToUpper(),
                                                                              Vertical = p.Vertical.ToUpper(),
                                                                              SubVertical = p.SubVertical.ToUpper(),
                                                                              Practice = p.Practice.ToUpper(),
                                                                              SubPractice = p.SubPractice.ToUpper(),
                                                                              ProfitCenter = p.ProfitCenter.ToUpper(),
                                                                              Gender = p.Title.ToUpper(),
                                                                              DeliveryManager = p.DeliveryManager.ToUpper(),
                                                                              EngagementType = p.EngagementType.ToUpper()
                                                                          }).ToListAsync();
                return listOfData;

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion


        #region IsValidEmployee

        /// <summary>
        /// Get competency of the user
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<bool> IsValidEmployee(string MID)
        {
            try
            {
                if (MID.StartsWith('M'))
                {
                    MID = MID.Substring(1);
                }
                var employee = await _context.TblCapacityReportStaging.Where(x => x.EmployeeId == MID).FirstOrDefaultAsync();
                if (employee != null)
                    return true;
                else
                    return false;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


        #region GetCompetencyBasedOnMID
        /// <summary>
        /// Get competency of the user
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<string> GetCompetencyBasedOnMID(string MID)
        {
            try
            {
                if (MID.StartsWith('M'))
                {
                    MID = MID.Substring(1);
                }
                var competency = await _context.TblCapacityReportStaging.Where(x => x.EmployeeId == MID).Select(y => y.Competency).FirstOrDefaultAsync();
                return competency;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region GetNameBasedOnMid
        /// <summary>
        /// Get Name based on MID
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<string> GetNameBasedOnMID(string MID)
        {
            try
            {
                if (MID.StartsWith('M'))
                {
                    MID = MID.Substring(1);
                }
                var name = await _context.TblCapacityReportStaging.Where(x => x.EmployeeId == MID).Select(y => y.Name).FirstOrDefaultAsync();
                return name;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


        #region GetGlobalSearchResult
        /// <summary>
        /// Get capacity data that matches the entered key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<List<CapacityModelDTO>> GetGlobalSearchResult(string key)
        {
            try
            {
                string MIDSearchKey;
                if (key.StartsWith("M") || key.StartsWith("m"))
                {
                    MIDSearchKey = key.Substring(1);
                }
                else
                {
                    MIDSearchKey = key;
                }
                List<CapacityModelDTO> data = this._context.TblCapacityReportStaging.Where(x => x.EmployeeId.Contains(MIDSearchKey) || x.DeliveryManager.Contains(key)
                                                           || x.Name.Contains(key) || x.ProjectName.Contains(key) || x.Location.Contains(key)
                                                           || x.GivenRole.Contains(key) || x.BillableStatus.Contains(key) || x.Practice.Contains(key) ||
                                                           x.SubPractice.Contains(key) || x.Competency.Contains(key)).Select(p => new CapacityModelDTO
                                                           {
                                                               EmployeeID = p.EmployeeId.ToUpper(),
                                                               Name = p.Name.ToUpper(),
                                                               Competency = p.Competency.ToUpper(),
                                                               ProjectName = p.ProjectName.ToUpper(),
                                                               GivenRole = p.GivenRole.ToUpper(),
                                                               Location = p.Location.ToUpper(),
                                                               OnOffShore = p.OnOffShore,
                                                               BillableStatus = p.BillableStatus.ToUpper(),
                                                               Vertical = p.Vertical.ToUpper(),
                                                               SubVertical = p.SubVertical.ToUpper(),
                                                               Practice = p.Practice.ToUpper(),
                                                               SubPractice = p.SubPractice.ToUpper(),
                                                               ProfitCenter = p.ProfitCenter.ToUpper(),
                                                               Gender = p.Title.ToUpper(),
                                                               DeliveryManager = p.DeliveryManager.ToUpper(),
                                                               EngagementType = p.EngagementType.ToUpper()
                                                           }).ToList<CapacityModelDTO>();

                return data;
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        public async Task<CapacityModelDTO> GetUserProfileDetails(string MID, string ConnectionString)
        {
            try
            {
                var profileDetails = new CapacityModelDTO();
                using (IDbConnection con = new SqlConnection(ConnectionString))
                {
                    string sQuery = "SELECT * FROM tblCapacityReport"
                                   + " WHERE EmployeeID = @EmployeeID";
                    con.Open();
                    profileDetails = con.QueryFirstOrDefaultAsync<CapacityModelDTO>(sQuery, new { EmployeeID = MID }).Result;
                }

                return profileDetails;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
