﻿/***************************************************************************************************************************
* Page Name  : AuditLogRepository.cs
* Created By : Supraja Harish  
* Created On : 10 Dec 2018
* Description : Method to save logs contanining login details for auditing
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class AuditLogRepository : IAuditLogRepository
    {
        #region private variables 
        private readonly CapacityInsightsDBContext _context;
        #endregion
        #region constructor 
        public AuditLogRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }
        #endregion
        #region SaveLoginHistory
        /// <summary>
        /// to save selected preferences
        /// </summary>
        /// <param name="preferences"></param>
        /// <returns></returns>
        public async Task SaveLoginHistory(AuditLogDTO auditLogDTO)
        {
            try
            {
                this._context.TblAuditLog.Add(new Entities.DataEntities.TblAuditLog
                {
                    EmployeeId = auditLogDTO.EmployeeID,
                    EmployeeName = auditLogDTO.EmployeeName,
                    UserAction = auditLogDTO.UserAction,
                    Reference = auditLogDTO.Reference,
                    //Id = 1
                    //UploadedDate = DateTime.Now
                });
                await this._context.SaveChangesAsync();
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

    }
}
