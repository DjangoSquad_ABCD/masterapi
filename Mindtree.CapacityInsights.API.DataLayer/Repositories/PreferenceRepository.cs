﻿/***************************************************************************************************************************
* Page Name  : PreferenceRepository.cs
* Created By :  
* Created On : 
* Description : Preference related API's DAL layer.
* Modified By  : Supraja Harish  
* Modified Date :  02/07/2019  
* Modified Reason: Code comments
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class PreferenceRepository : IPreferenceRepository
    {
        #region private variables 
        /// <summary>
        /// DB context instatiation
        /// </summary>
        private readonly CapacityInsightsDBContext _context;
        #endregion

        #region Constructor 
        public PreferenceRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }
        #endregion

        #region SavePreferences
        /// <summary>
        /// to save selected preferences
        /// </summary>
        /// <param name="preferencesModelDTO">Preference model DTO</param>
        /// <returns>True if saved successfully else false</returns>
        public async Task<bool> SavePreferences(PreferencesModelDTO preferencesModelDTO)
        {
            try
            {
                string pref = JsonConvert.SerializeObject(preferencesModelDTO.Preferences);

                //check if preference name exists already
                var defaultExist = _context.TblProfilePrefrence.Where(a => a.DefaultPreference == true && a.Mid == preferencesModelDTO.Mid).ToList();
                if (defaultExist.Count()!=0 && preferencesModelDTO.DefaultPreference == true)
                {
                    foreach (var def in defaultExist)
                    {
                        def.DefaultPreference = false;
                    }
                    await _context.SaveChangesAsync();
                }
                //save users preference
                _context.TblProfilePrefrence.Add(new Entities.DataEntities.TblProfilePrefrence
                {
                    FilterName = preferencesModelDTO.FilterName,
                    Preferences = pref,
                    Mid = preferencesModelDTO.Mid,
                    DefaultPreference = preferencesModelDTO.DefaultPreference,
                    Uploadedby = preferencesModelDTO.Mid,
                    UploadedDate = DateTime.Now
                });
                var res= await _context.SaveChangesAsync();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetTblProfilePreference
        /// <summary>
        /// To get all preference data from db.
        /// </summary>
        /// <param name="MID">MID of the current user</param>
        /// <returns>List of preferences for the user</returns>
        public async Task<List<PreferencesModelDTO>> GetTblProfilePreference(string MID)
        {
            try
            {
                //Get all prefrences saved by a particular user based on MID
                var preferenceList =await _context.TblProfilePrefrence.Where(x=>x.Mid==MID).Select(p => new PreferencesModelDTO
                {
                    Mid = p.Mid,
                    FilterName = p.FilterName,
                    Preferences = JsonConvert.DeserializeObject<FilterModelDTO>(p.Preferences.ToUpper()),
                    UploadedDate = p.UploadedDate,
                    Uploadedby = p.Uploadedby
                }).ToListAsync();
                return preferenceList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetDefaultPreference
        /// <summary>
        /// To get default preference if any 
        /// </summary>
        /// <param name="MID">ID of the current user</param>
        /// <returns>Preference model DTO</returns>
        public async Task<PreferencesModelDTO> GetDefaultPreference(string MID)
        {
            try
            {
                //Get default preference name and filter details from tblProfilePreference table
                PreferencesModelDTO defaultPreference = _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID) && a.DefaultPreference == true).Select(
                                                                                                            a => new PreferencesModelDTO
                                                                                                            {
                                                                                                                FilterName = a.FilterName,
                                                                                                                Preferences = JsonConvert.DeserializeObject<FilterModelDTO>(a.Preferences.ToUpper())
                                                                                                           }).FirstOrDefault();

                //If there is no default preference then select first preference as default.
                if (defaultPreference==null)
                {
                    defaultPreference = _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID)).Select(
                                                            a => new PreferencesModelDTO
                                                            {
                                                                FilterName = a.FilterName,
                                                                Preferences = JsonConvert.DeserializeObject<FilterModelDTO>(a.Preferences.ToUpper())
                                                            }).FirstOrDefault();
                }
                return defaultPreference;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetDefaultPreferenceForDisplay
        /// <summary>
        /// To get default preference if any or return null
        /// </summary>
        /// <param name="MID">ID of the current user</param>
        /// <returns>Preferences Model DTO</returns>
        public async Task<PreferencesModelDTO> GetDefaultPreferenceForDisplay(string MID)
        {
            try
            {

                //Get default preference name and filter details from tblProfilePreference table
                PreferencesModelDTO defaultPreference = await _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID) && a.DefaultPreference == true).Select(
                   a => new PreferencesModelDTO
                   {
                       FilterName = a.FilterName,
                       Preferences = JsonConvert.DeserializeObject<FilterModelDTO>(a.Preferences.ToUpper())
                   }).FirstOrDefaultAsync();

                //If there is no default preference then return null.
                if (defaultPreference == null)
                {
                    return null;
                    // defaultPreference = _context.TblProfilePrefrence.Where(a => a.Mid.Equals(MID)).Select(a => a.FilterName).FirstOrDefault();
                }
                return defaultPreference;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region UpdateDefaultPreference
        /// <summary>
        /// changes/updates the defalut prefernce to a new selected one
        /// </summary>
        /// <param name="preference">Preferences Model DTO</param>
        /// <returns></returns>
        public async Task UpdateDefaultPreference(PreferencesModelDTO preference)
        {
            try
            {

                //Get the previous default preference details from tblProfilePrefrence
                var PreviousDefaultPreference = _context.TblProfilePrefrence.Where(x => x.Mid == preference.Mid).
                    Where(x => x.DefaultPreference == true).FirstOrDefault();

                
                //If previous preference is not null
                if(PreviousDefaultPreference!=null)
                {
                    //If previous preference is same as current preference then don't update the preference.
                    if (PreviousDefaultPreference.FilterName == preference.FilterName)
                        return;

                    //If not then update the previous preference default preference's default column as false
                    PreviousDefaultPreference.DefaultPreference = false;
                   await  _context.SaveChangesAsync();
                }

                //Update the selected preference value as default.
                var PreferenceToBeMadeDefault = _context.TblProfilePrefrence.Where(x => x.Mid == preference.Mid).
                    Where(x => x.FilterName == preference.FilterName).FirstOrDefault();
                if(PreferenceToBeMadeDefault==null)
                {
                    throw new  InvalidOperationException();
                }
                else
                {
                    PreferenceToBeMadeDefault.DefaultPreference = true;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

    }
}
