﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class ThemeRepository : IThemeRepository
    {
        #region private variables 
        /// <summary>
        /// DB context initialization variable
        /// </summary>
        private readonly CapacityInsightsDBContext _context;
        #endregion

        #region constructor 
        public ThemeRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }
        #endregion

        #region SaveTheme
        /// <summary>
        /// to save the theme selected by the user
        /// </summary>
        /// <param name="themeDTO">Theme DTO object</param>
        /// <returns>true or false</returns>
        public async Task<Boolean> SaveTheme(ThemeDTO themeDTO)
        {
            try
            {

                //Get the previous default theme details from tblSaveTheme
                var previousTheme = _context.TblSaveTheme.Where(x => x.MID == themeDTO.MID).FirstOrDefault();


                //If previous theme is not null
                if (previousTheme != null)
                {
                    //If previous theme is same as current theme then don't update.
                    if (previousTheme.Theme == themeDTO.Theme)
                        return true;

                    //If not then update the theme.
                    previousTheme.Theme = themeDTO.Theme;
                    await _context.SaveChangesAsync();
                    return true;
                }
                //If there is no theme saved for the current user then add a new entry to save theme table
                else
                { 
                    this._context.TblSaveTheme.Add(new Entities.DataEntities.TblSaveTheme
                    {
                        MID = themeDTO.MID,
                        Theme = themeDTO.Theme
                    });
                    await this._context.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetTheme
        /// <summary>
        /// Get theme of saved by the user
        /// </summary>
        /// <param name="MID">MID of current user</param>
        /// <returns>Theme DTO object</returns>
        public async Task<ThemeDTO> GetTheme(string MID)
        {
            try
            {
                //Get theme details from tblSaveTheme for current user
                var userTheme = await _context.TblSaveTheme.Where(x => x.MID == MID).Select(a => new ThemeDTO
                {
                    MID = a.MID,
                    Theme = a.Theme
                }).FirstOrDefaultAsync();
                return userTheme;
            }
            catch(Exception)
            {
                throw;
            }

        }
        #endregion
    }
}
