﻿/***************************************************************************************************************************
* Page Name  : DashBoardRepository.cs
* Created By : Madhushree Sv 
* Created On : 28 Nov 2018
* Description : Dashboard DAL
* Modified By  :  
* Modified Date :  
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using System.Linq;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class DashBoardRepository: IDashBoardRepository
    {
        #region private variables 
        private readonly CapacityInsightsDBContext _context;
        #endregion
        #region constructor 
        public DashBoardRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }
        #endregion
        #region GetCompetencyBuildingCount
        /// <summary>
        /// This is controller get method to get Competency building count
        /// </summary>
        /// <returns>Competency building</returns>
        public async Task<int> GetCompetencyBuildingCount()
        {
            try
            {
                //Get count of all minds who are in BillableStatus of Competnecy Building
                int competencyBuildingCount =await _context.TblCapacityReportStaging.Where(i=>i.BillableStatus == "Competency building").Select(p => p.EmployeeId).Distinct().CountAsync(); 
                return competencyBuildingCount;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion
        #region GetMindtreemindsCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count
        /// </summary>
        /// <returns>Mindtree minds count</returns>
        public async Task<int> GetMindtreemindsCount()
        {
            try
            {
                //Get count of all mindtree minds
                int mindtreemindsCount = await _context.TblCapacityReportStaging.Select(p => p.EmployeeId).Distinct().CountAsync();
                return mindtreemindsCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region GetDeployableCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count who are deployable
        /// </summary>
        /// <returns>Deployable Mindtree minds count</returns
        public async Task<int> GetDeployableCount()
        {
            try
            {
                //Get count of all deployable mindtree minds
                int deployableCount =await _context.TblCapacityReportStaging.Where(i => i.BillableStatus == "Deployable").Select(p => p.EmployeeId).Distinct().CountAsync(); 
                return deployableCount;
            }
            catch (Exception )
            {
                throw ;
            }
        }
        #endregion
        #region GetUnBilledMindsCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count who are unbilled
        /// </summary>
        /// <returns>UnBilled minds count</returns
        public async Task<int> GetUnBilledMindsCount()
        {
            try
            {
                //Get count of all unbilled mindtree minds
                int unbilledMindsCount = await _context.TblCapacityReportStaging.Where(i => i.BillableStatus == "UnBilled").Distinct().Select(p => p.EmployeeId).Distinct().CountAsync(); 
                return unbilledMindsCount;
            }
            catch (Exception )
            {
                throw ;
            }
        }
        #endregion
        #region GetBilledMindsCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count who are billed
        /// </summary>
        /// <returns>Billed minds count</returns
        public async Task<int> GetBilledMindsCount()
        {
            try
            {
                //Get count of all billed mindtree minds
                int billedMindsCount = await _context.TblCapacityReportStaging.Where(i => i.BillableStatus == "Billed").Distinct().Select(p => p.EmployeeId).Distinct().CountAsync(); 
                return billedMindsCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region GetOnOffShoreCount 
        /// <summary>
        /// Get the onsite offshore count
        /// </summary>
        /// <returns>List of total count,onsite count,offshore count and other data count</returns>
        public async Task<List<int>> GetOnOffShoreCount()
        {
            try
            {
                //variables
                int totalData = 0;
                int onsiteData = 0;
                int offshoreData = 0;
                int otherData = 0;
                List<int> listOnOffshore = new List<int>();

                //total mindtree mind's count
                totalData = await _context.TblCapacityReportStaging.CountAsync();
                //mindtree mind's count at onsite
                onsiteData = await _context.TblCapacityReportStaging.Where(a => a.OnOffShore == "Onsite").Select(p => p.EmployeeId).Distinct().CountAsync(); 
                //mindtree mind's count at offshore
                offshoreData = await _context.TblCapacityReportStaging.Where(a => a.OnOffShore == "OffShore").Select(p => p.EmployeeId).Distinct().CountAsync(); 
                
                //check for empty and null values
                otherData = (totalData - (onsiteData + offshoreData));

                listOnOffshore.Add(totalData);
                listOnOffshore.Add(onsiteData);
                listOnOffshore.Add(offshoreData);
                listOnOffshore.Add(otherData);

                return listOnOffshore;

            }
            catch (Exception)
            {

                throw;
            }
            
        }
        #endregion
        #region GetMaleFemaleCount
        /// <summary>
        /// Get the male female count
        /// </summary>
        /// <returns>List of total count,male count,female count </returns>
        public async Task<List<int>> GetMaleFemaleCount()
        {
            try
            {
                //variables
                int totalData = 0;
                int maleData = 0;
                int femaleData = 0;
               // int otherData = 0;
                List<int> listMaleFemale = new List<int>();

                //total mindtree mind's count
                totalData = await _context.TblCapacityReportStaging.CountAsync();
                //male mindtree mind's count
                maleData = await _context.TblCapacityReportStaging.Where(a => a.Title == "MEN").Select(p => p.EmployeeId).Distinct().CountAsync();
                //female mindtree mind's count
                femaleData =await _context.TblCapacityReportStaging.Where(a => a.Title == "WOMEN").Select(p => p.EmployeeId).Distinct().CountAsync();

                listMaleFemale.Add(totalData);
                listMaleFemale.Add(maleData);
                listMaleFemale.Add(femaleData);
                return listMaleFemale;

            }
            catch (Exception)
            {

                throw ;
            }

        }
        #endregion
        #region GetEngagementTypeCount
        /// <summary>
        /// Get the Engagement Type
        /// </summary>
        /// <returns>List of not listed,HYB,TNM,FPC,FMC </returns>
        public async Task<List<int>> GetEngagementTypeCount()
        {
            try
            {
                //variables
                int totalData = 0;
                int notListedData = 0;
                int HYBData = 0;
                int TNMData = 0;
                int FPCData = 0;
                int FMCData = 0;
             
                List<int> listEngagementType = new List<int>();
                //total mindtree mind's count
                totalData =await _context.TblCapacityReportStaging.Select(p => p.EmployeeId).Distinct().CountAsync();
                //mindtree mind's count at HYB
                HYBData = await _context.TblCapacityReportStaging.Where(a => a.EngagementType == "HYB").Select(p => p.EmployeeId).Distinct().CountAsync();
                //mindtree mind's count at TNM
                TNMData = await _context.TblCapacityReportStaging.Where(a => a.EngagementType == "TNM").Select(p => p.EmployeeId).Distinct().CountAsync();
                //mindtree mind's count at FPC
                FPCData = await _context.TblCapacityReportStaging.Where(a => a.EngagementType == "FPC").Select(p => p.EmployeeId).Distinct().CountAsync();
                //mindtree mind's count at FMC
                FMCData = await _context.TblCapacityReportStaging.Where(a => a.EngagementType == "FMC").Select(p => p.EmployeeId).Distinct().CountAsync();
                //mindtree mind's count which are not listed
                notListedData = await _context.TblCapacityReportStaging.Where(a => a.EngagementType == "NULL").Select(p => p.EmployeeId).Distinct().CountAsync();

                listEngagementType.Add(totalData);
                listEngagementType.Add(notListedData);
                listEngagementType.Add(HYBData);
                listEngagementType.Add(TNMData);
                listEngagementType.Add(FPCData);
                listEngagementType.Add(FMCData);              

                return listEngagementType;

            }
            catch (Exception )
            {

                throw;
            }

        }
        #endregion
        #region GetDeliveryManager
        /// <summary>
        /// Get Delivery Manager
        /// </summary>
        /// <returns></returns>
        public async Task<List<DeliveryManagerCount>> GetDeliveryManager()
        {
            try
            {
                
                    //variables
                    var results =await _context.TblCapacityReportStaging.GroupBy(m => new { m.DeliveryManager }).Select(
                        a => new DeliveryManagerCount { DeliveryManger = a.Key.DeliveryManager, DeliveryMangerCount = a.Count()}).ToListAsync();
                    return results;
                
                
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region GetVerticalCount
        /// <summary>
        /// Get Vertical Count
        /// </summary>
        /// <returns></returns>
        public async Task<List<VerticalWiseCount>> GetVerticalCount()
        {
            try
            {
                //int i = 0;
              
                    //variables
                    var results =await _context.TblCapacityReportStaging.GroupBy(m => new { m.Vertical }).Select(
                        a => new VerticalWiseCount { Vertical = a.Key.Vertical, VerticalCount = a.Count()}).OrderByDescending(k=>k.VerticalCount).
                        ToListAsync();

                    //edit the null value to NotAvailable
                    foreach (var item in results)
                    {
                        if (item.Vertical == null)
                            item.Vertical = "Not Listed";
                    }

                    return results;
              

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
