﻿/***************************************************************************************************************************
* Page Name  : RoleRepository.cs
* Created By : Hanisha Hanumula 
* Created On : 5 Nov 2018
* Description : role repository 
* Modified By  :   
* Modified Date :   
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using System.Linq;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Microsoft.EntityFrameworkCore;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        #region private variables 
        private readonly CapacityInsightsDBContext _context;
        #endregion

        #region Constructor
        public RoleRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
        }
        #endregion

        #region SaveAuditLog
        /// <summary>
        /// to save user footprint
        /// </summary>
        /// <param name="auditLogDTO"></param>
        /// <returns></returns>
        public async Task<Boolean> SaveAuditLog(AuditLogDTO auditLogDTO)
        {
            try
            {
                //adds user footpeint model to database
                _context.TblAuditLog.Add(new TblAuditLog
                {
                    EmployeeId = auditLogDTO.EmployeeID,
                    EmployeeName = auditLogDTO.EmployeeName,
                    UserAction = auditLogDTO.UserAction,
                    Reference = auditLogDTO.Reference,
                    LastUpdatedOn = DateTime.Now,
                    UploadedDateTime = DateTime.Now
                });
                // saves the applied changes to db
                await _context.SaveChangesAsync();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRole
        /// <summary>
        /// Method to get role by MID
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<int> GetRole(string MID)
        {
            try
            {
                // returns role id based on mid
                int roleid = await _context.TblSaveRole.Where(y => y.MID == MID).Select(y => y.RID).FirstOrDefaultAsync();
                //int gtRole = Convert.ToInt16(roleid);
                return roleid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAllAdmins
     
        /// <summary>
        ///  Method to get all roles
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserModelDTO>> GetAllAdmins()
        {
            //try
            //{
            //    List<string> lstAdmins = new List<string>();
            //    lstAdmins = await _context.TblSaveRole.Where(a=> !string.IsNullOrEmpty(a.MID) && a.RID==1).Select(a=>a.MID).Distinct().ToListAsync();
            //    return lstAdmins;
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}
            try
            {
                List<string> lstAdmins = new List<string>();
                List<string> listAdminDetails = new List<string>();
                // gets list of admins
                lstAdmins = await _context.TblSaveRole.Where(a => !string.IsNullOrEmpty(a.MID) && a.RID == 1).Select(a => a.MID).Distinct().ToListAsync();
                List<UserModelDTO> lstAdmiDetails = new List<UserModelDTO>();

                //removing m from mids if exists
                foreach (var mid in lstAdmins)
                {
                    string MIDSearchKey;
                    if (mid.StartsWith("M") || mid.StartsWith("m"))
                    {
                        MIDSearchKey = mid.Substring(1);
                    }
                    else
                    {
                        MIDSearchKey = mid;
                    }
                    // fetching data from capacity report table
                    UserModelDTO UserModelObj = await _context.TblCapacityReport.Where(a => a.EmployeeId.Equals(MIDSearchKey)).Select(
                        p => new UserModelDTO
                        {
                            EmployeeID = p.EmployeeId,
                            Name = p.Name,
                            Competency = p.Competency
                        }).FirstOrDefaultAsync();
                    if (UserModelObj!=null)
                    { 
                    lstAdmiDetails.Add(UserModelObj);
                    }
                }
                return lstAdmiDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region CreateRole
        /// <summary>
        ///  Method to create role, also check if the MID is duplicate or not
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<Boolean> CreateRole(RoleModelDTO role)
        {

            try
            {
                var admin= await _context.TblSaveRole.Where(x => x.MID == role.MID && x.RID ==1).FirstOrDefaultAsync();
               
                if (admin == null)
                {
                    var roleid = _context.TblRole.Select(x => x.GivenRole == role.Role).FirstOrDefault();
                    _context.TblSaveRole.Add(new Entities.DataEntities.TblSaveRole
                    {
                        Id = Guid.NewGuid(),
                        MID = role.MID,
                        RID = Convert.ToInt32(roleid),
                        ActiveInactive = 1,
                        Uploadedby = role.MID,
                        UploadedDate = DateTime.Now
                    });
                    await _context.SaveChangesAsync();
                    return true;
                }

                return false;
            } 
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region DeleteRole
        /// <summary>
        /// Method to delete role, also check if the MID is duplicate or not
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<Boolean> DeleteRole(UserModelDTO role)
        {

            try
            {
                    TblSaveRole tblSaveRoleObj = _context.TblSaveRole.Where(x => x.MID == role.EmployeeID).FirstOrDefault();
                if(tblSaveRoleObj==null)
                {
                    return false;
                }
                    _context.TblSaveRole.Remove(tblSaveRoleObj);
                    await _context.SaveChangesAsync();
                   

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region verify admin role
        /// <summary>
        /// verify admin role
        /// </summary>
        /// <param name="MidOfLoggedInUser"></param>
        /// <returns></returns>
        public async Task<Boolean> VerifyAdminRole(string MidOfLoggedInUser)
        {
            //variable to store if user is admin or not
            bool IsAdmin;
            // gets amin role id from db
            var roleIDOfUser = _context.TblRole.Where(x => x.GivenRole == "Admin").Select(x => x.RId).FirstOrDefault();
            // checks weather the user is admin or not
            var admin = _context.TblSaveRole.Where(x => x.RID == roleIDOfUser && x.MID == MidOfLoggedInUser).FirstOrDefault();
            if(admin ==null)
            {
                return false;
            }
            return true;

        }
        #endregion
    }
}

