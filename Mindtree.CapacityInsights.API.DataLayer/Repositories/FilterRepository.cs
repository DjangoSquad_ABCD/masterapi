﻿/***************************************************************************************************************************
* Page Name  : FilterRepository.cs
* Created By : Rakshitha R  
* Created On : 1 Dec 2018
* Description : Methods to get Filtered data
* Modified By  :  Supraja Harish, Ayushi Khandelwal
* Modified Date :  18 Jan 2019
* Modified Reason: Modified existing methods
***************************************************************************************************************************/


using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using System.Linq;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Microsoft.EntityFrameworkCore;

namespace Mindtree.CapacityInsights.API.DataLayer.Repositories
{
    public class FilterRepository : IFilterRepository
    {
        //read only variables
        private readonly CapacityInsightsDBContext _context;
        /// <summary>
        /// 
        /// </summary>
        private List<TblCapacityReportStaging> capacityResultList;
        //constructor of FilterController
        public FilterRepository(CapacityInsightsDBContext context)
        {
            this._context = context;
            capacityResultList = _context.TblCapacityReportStaging.ToList();
        }

        /// <summary>
        /// Fetching all the DeliveryPartners, DeliveryManagers, ProfitCenters, ProjectNames, AccountGroups, Practices
        /// <returns></returns> 
        public async Task<FilterModelDTO> GetAllFirstFilterOptions()
        {
            try
            {
                // to  get all first filters
                FilterModelDTO firstFilter = new FilterModelDTO();
                var abc= capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.DeliveryPartner != null).Select(i => new { i.DeliveryManager, i.DeliveryPartner,i.ProfitCenter,i.ProjectName,i.AccountGroup,i.BillableStatus,i.Srstatus,i.EngagementType,i.Location }).Distinct().ToArray();

                firstFilter.DeliveryPartner = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.DeliveryPartner != null).Select(a => a.DeliveryPartner).Distinct().ToArray();
                firstFilter.DeliveryManager = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.DeliveryManager != null).Select(a => a.DeliveryManager).Distinct().ToArray();
                firstFilter.ProfitCenter = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.ProfitCenter != null).Select(a => a.ProfitCenter).Distinct().ToArray();
                firstFilter.ProjectName = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.ProjectName != null).Select(a => a.ProjectName).Distinct().ToArray();
                firstFilter.AccountGroup = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.AccountGroup != null).Select(a => a.AccountGroup).Distinct().ToArray();
                firstFilter.Practice = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Practice != null).Select(a => a.Practice).Distinct().ToArray();
                return firstFilter;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Fetching all the BillableStatuses,Locations,SRStatuses, EngagemeentTypes
        /// <returns></returns>   
        public async Task<FilterModelDTO> GetAllSecondFilterOptions()
        {
            try
            {
                //to get all second filters
                FilterModelDTO secondFilter = new FilterModelDTO();


                secondFilter.BillableStatus = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.BillableStatus != null).Select(a => a.BillableStatus).Distinct().ToArray();
                secondFilter.Location = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Location != null).Select(a => a.Location).Distinct().ToArray();
                secondFilter.SRStatus = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Srstatus != null).Select(a => a.Srstatus).Distinct().ToArray();
                secondFilter.EngagementCode = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.EngagementType != null).Select(a => a.EngagementType).Distinct().ToArray();
                secondFilter.Gender = capacityResultList.Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Title != null).Select(a => a.Title).Distinct().ToArray();
                return secondFilter;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region interRelatedFilters

        /// <summary>
        /// Some of the filters are inter related for example if Profit center is selected as BFSI,
        /// then account names filter must have only BFSI accounts.
        /// </summary>
        /// <param name="interRelatedFilter">The user selected filters are passed.</param>
        /// <returns>Filter Model object</returns>
        public async Task<FilterModelDTO> GetInterrelatedFilters(FilterModelDTO interRelatedFilter)
        {
            FilterModelDTO filterOnSelectedFilters = new FilterModelDTO();
            //Load all filters based on selected ProfitCenter
            filterOnSelectedFilters.ProfitCenter = capacityResultList.Where(x =>
                                                       (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                       (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                       (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                       (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                       (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                       (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                       (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                       (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                       (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                       (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                        .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.ProfitCenter != null)
                                                        .Select(p => p.ProfitCenter).Distinct().ToArray();



            //Load all filters based on selected DeliveryPartner
            filterOnSelectedFilters.DeliveryPartner = capacityResultList.Where(x =>
                                                          (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                          (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                          (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                          (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                          (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                          (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                          (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                          (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                          (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                          (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                         .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.DeliveryPartner != null)
                                                         .Select(p => p.DeliveryPartner).Distinct().ToArray();



            //Load all filters based on selected DeliveryManager
            filterOnSelectedFilters.DeliveryManager = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                         .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.DeliveryManager != null)
                                                          .Select(p => p.DeliveryManager).Distinct().ToArray();



            //Load all filters based on selected ProjectName
            filterOnSelectedFilters.ProjectName = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                         .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.ProjectName != null)
                                                          .Select(p => p.ProjectName).Distinct().ToArray();





            //Load all filters based on selected AccountGroup
            filterOnSelectedFilters.AccountGroup = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.AccountGroup != null)
                                                          .Select(p => p.AccountGroup).Distinct().ToArray();



            //Load all filters based on selected Practice
            filterOnSelectedFilters.Practice = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Practice != null)
                                                          .Select(p => p.Practice).Distinct().ToArray();




            //Load all filters based on selected BillableStatus
            filterOnSelectedFilters.BillableStatus = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.BillableStatus != null)
                                                          .Select(p => p.BillableStatus).Distinct().ToArray();



            //Load all filters based on selected Location
            filterOnSelectedFilters.Location = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Location != null)
                                                          .Select(p => p.Location).Distinct().ToArray();



            //Load all filters based on selected SRStatus
            filterOnSelectedFilters.SRStatus = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.Srstatus != null)
                                                          .Select(p => p.Srstatus).Distinct().ToArray();




            //Load all filters based on selected EngagementCode
            filterOnSelectedFilters.EngagementCode = capacityResultList.Where(x =>
                                                           (interRelatedFilter.ProfitCenter.Count() == 0 || (x.ProfitCenter != null && interRelatedFilter.ProfitCenter.Contains(x.ProfitCenter))) &&
                                                           (interRelatedFilter.DeliveryPartner.Count() == 0 || (x.DeliveryPartner != null && interRelatedFilter.DeliveryPartner.Contains(x.DeliveryPartner))) &&
                                                           (interRelatedFilter.DeliveryManager.Count() == 0 || (x.DeliveryManager != null && interRelatedFilter.DeliveryManager.Contains(x.DeliveryManager))) &&
                                                           (interRelatedFilter.ProjectName.Count() == 0 || (x.ProjectName != null && interRelatedFilter.ProjectName.Contains(x.ProjectName))) &&
                                                           (interRelatedFilter.AccountGroup.Count() == 0 || (x.AccountGroup != null && interRelatedFilter.AccountGroup.Contains(x.AccountGroup))) &&
                                                           (interRelatedFilter.Practice.Count() == 0 || (x.Practice != null && interRelatedFilter.Practice.Contains(x.Practice))) &&
                                                           (interRelatedFilter.BillableStatus.Count() == 0 || (x.BillableStatus != null && interRelatedFilter.BillableStatus.Contains(x.BillableStatus))) &&
                                                           (interRelatedFilter.Location.Count() == 0 || (x.Location != null && interRelatedFilter.Location.Contains(x.Location))) &&
                                                           (interRelatedFilter.SRStatus.Count() == 0 || (x.Srstatus != null && interRelatedFilter.SRStatus.Contains(x.Srstatus))) &&
                                                           (interRelatedFilter.EngagementCode.Count() == 0 || (x.EngagementType != null && interRelatedFilter.EngagementCode.Contains(x.EngagementType))))
                                                          .Where(a => !string.IsNullOrEmpty(a.EmployeeId) && a.EngagementType != null)
                                                          .Select(p => p.EngagementType).Distinct().ToArray();


            return filterOnSelectedFilters;


        }

        #endregion


    }
}




