﻿/***************************************************************************************************************************
* Page Name  : CapacityInsightsDBContext.cs
* Created By : Adabala Venkat Sujit 
* Created On : 5 Nov 2018
* Description : scaffolded db context
* Modified By  :   
* Modified Date :   
* Modified Reason:  
***************************************************************************************************************************/

using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Mindtree.CapacityInsights.API.Entities.DataEntities;

namespace Mindtree.CapacityInsights.API.DataLayer
{
    public partial class CapacityInsightsDBContext : DbContext
    {
        #region Constructor
        public CapacityInsightsDBContext()
        {
        }

        public CapacityInsightsDBContext(DbContextOptions<CapacityInsightsDBContext> options)
            : base(options)
        {
        }
        #endregion

        #region regestring classes for ORM
        public virtual DbSet<TblAuditLog> TblAuditLog { get; set; }
        public virtual DbSet<TblCapacityReport> TblCapacityReport { get; set; }
        public virtual DbSet<TblExceptionLog> TblExceptionLog { get; set; }
        public virtual DbSet<TblProfilePrefrence> TblProfilePrefrence { get; set; }
        public virtual DbSet<TblRole> TblRole { get; set; }
        public virtual DbSet<TblSaveRole> TblSaveRole { get; set; }
        public virtual DbSet<TblSaveTheme> TblSaveTheme { get; set; }
        public virtual DbSet<TblCapacityReportStaging> TblCapacityReportStaging { get; set; }
        #endregion

        #region ORM Logic
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.; Database=CapacityInsightsDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblAuditLog>(entity =>
            {
                entity.ToTable("tblAuditLog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.Reference).IsUnicode(false);

                entity.Property(e => e.ReqId)
                    .HasColumnName("ReqID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UploadedBy).HasMaxLength(32);

                entity.Property(e => e.UploadedDateTime).HasColumnType("datetime");

                entity.Property(e => e.UploadedFor).HasMaxLength(32);

                entity.Property(e => e.UserAction)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCapacityReport>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("tblCapacityReport");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.ConfirmedOn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IgdeliveryHead).HasColumnName("IGDeliveryHead");

                entity.Property(e => e.IjpappliedDate).HasColumnName("IJPAppliedDate");

                entity.Property(e => e.IjpappliedSrid).HasColumnName("IJPAppliedSRID");

                entity.Property(e => e.Ijpstuatus).HasColumnName("IJPStuatus");

                entity.Property(e => e.IsDataCorrect).HasDefaultValueSql("((1))");

                entity.Property(e => e.Klocation).HasColumnName("KLocation");

                entity.Property(e => e.LastUpdatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedOn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Rtpf).HasColumnName("RTPF");

                entity.Property(e => e.Srstatus).HasColumnName("SRStatus");

                entity.Property(e => e.StaffingRequestId).HasColumnName("StaffingRequestID");
            });

            modelBuilder.Entity<TblCapacityReportStaging>(entity =>
            {
                entity.HasKey(e => e.RowId);

                entity.ToTable("tblCapacityReport_Staging");

                entity.Property(e => e.RowId).HasColumnName("RowID");

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(50);

                entity.Property(e => e.IgdeliveryHead).HasColumnName("IGDeliveryHead");

                entity.Property(e => e.IjpappliedDate).HasColumnName("IJPAppliedDate");

                entity.Property(e => e.IjpappliedSrid).HasColumnName("IJPAppliedSRID");

                entity.Property(e => e.Ijpstuatus).HasColumnName("IJPStuatus");

                entity.Property(e => e.Klocation).HasColumnName("KLocation");

                entity.Property(e => e.LastUpdatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdatedOn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Rtpf).HasColumnName("RTPF");

                entity.Property(e => e.Srstatus).HasColumnName("SRStatus");

                entity.Property(e => e.StaffingRequestId).HasColumnName("StaffingRequestID");
            });

            modelBuilder.Entity<TblExceptionLog>(entity =>
            {
                entity.ToTable("tblExceptionLog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ExceptionLevel)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Source).IsUnicode(false);

                entity.Property(e => e.StackTrace).HasColumnType("text");

                entity.Property(e => e.TargetSite).IsUnicode(false);
            });

            

            modelBuilder.Entity<TblProfilePrefrence>(entity =>
            {
                entity.HasKey(e => new { e.Mid, e.FilterName});

                entity.ToTable("tblProfilePrefrence");

                entity.Property(e => e.Mid)
                             .HasColumnName("MID")
                             .HasMaxLength(50)
                             .ValueGeneratedNever().IsRequired();

                entity.Property(e => e.FilterName)
                             .HasColumnName("FilterName")
                             .HasMaxLength(400)
                             .ValueGeneratedNever().IsRequired()
                             ;


                entity.Property(e => e.UploadedDate).HasColumnType("datetime");

                entity.Property(e => e.Uploadedby)
                             .HasColumnName("Uploadedby")
                             .HasMaxLength(50)
                             .ValueGeneratedNever();
            });

            modelBuilder.Entity<TblRole>(entity =>
            {
                entity.HasKey(e => e.RId);

                entity.ToTable("tblRole");

                entity.Property(e => e.RId)
                    .HasColumnName("RID")
                    .HasMaxLength(10)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<TblSaveRole>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("tblSaveRole");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasMaxLength(10)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<TblSaveTheme>(entity =>
            {
                entity.HasKey(e => e.MID);

                entity.ToTable("tblSaveTheme");

                entity.Property(e => e.MID)
                          .HasColumnName("MID")
                          .HasMaxLength(50)
                          .ValueGeneratedNever().IsRequired();

                entity.Property(e => e.Theme)
                             .HasColumnName("Theme")
                             .HasMaxLength(400)
                             .ValueGeneratedNever().IsRequired()
                             ;
            });

        }
        #endregion
    }
}
