﻿using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IAuditLogBAL
    {
        /// <summary>
        /// save the login history by passing audit
        /// </summary>
        /// <param name="auditLogDTO"></param>
        /// <returns></returns>
        Task SaveLoginHistory(AuditLogDTO auditLogDTO);
    }
}
