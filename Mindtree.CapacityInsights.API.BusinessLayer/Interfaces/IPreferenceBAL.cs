﻿/***************************************************************************************************************************
* Page Name  : PreferenceBAL.cs
* Created By : 
* Created On : 
* Description : Preference API BAL interface methods.
* Modified By  : Supraja Harish   
* Modified Date : 02/07/2019  
* Modified Reason: Code comments
***************************************************************************************************************************/

using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IPreferenceBAL
    {
        /// <summary>
        /// Data layer call to save preferences
        /// </summary>
        /// <param name="preferencesModelDTO"></param>
        /// <returns></returns>
        Task<bool> SavePreferences(PreferencesModelDTO preferencesModelDTO);

        /// <summary>
        /// To get all capacity data from db
        /// </summary>
        /// <returns>List of capacity data</returns>
        Task<List<PreferencesModelDTO>> GetTblProfilePreference(string MID);

        /// <summary>
        /// To get default preference if any 
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        Task<PreferencesModelDTO> GetDefaultPreference(string MID);

        /// <summary>
        /// To get default preference for UI display
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        Task<PreferencesModelDTO> GetDefaultPreferenceForDisplay(String MID);


        
        /// <summary>
        /// method to change Defaultpreference selection
        /// </summary>
        /// <param name="preference"></param>
        /// <returns></returns>
        Task ChangeDefaultPreference(PreferencesModelDTO preference);
      

    }
}
