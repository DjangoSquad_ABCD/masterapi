﻿/***************************************************************************************************************************
* Page Name  : IFilterBAL.cs
* Created By : Rakshitha R  
* Created On : 1 Dec 2018
* Description : Methods to get Filtered data
* Modified By  :  Supraja Harish, Ayushi Khandelwal
* Modified Date :  18 Jan 2019
* Modified Reason: Modified existing methods
***************************************************************************************************************************/

using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IFilterBAL
    {
        /// <summary>
        /// Fetching all the DeliveryPartners,DeliveryManagers,ProfitCenters,ProjectNames,AccountGroups,Practices
        /// <returns></returns> 
        Task<FilterModelDTO> GetAllFirstFilterOptions();

        /// <summary>
        /// Fetching all the BillableStatuses,Locations,SRStatuses, EngagemeentTypes
        /// <returns></returns>               
        Task<FilterModelDTO> GetAllSecondFilterOptions();

        /// <summary>
        /// get the filtered profit center data 
        /// </summary>
        /// <param name="interRelatedFilter"></param>
        /// <returns></returns>
        Task<FilterModelDTO> GetInterrelatedFilters(FilterModelDTO interRelatedFilter);
    }                                         
}


