﻿/***************************************************************************************************************************
* Page Name  : CapacityController.cs
* Created By : Rakshitha R  
* Created On : 10 Dec 2018
* Description : Capacity report related API's are coded in this controller
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface ICapacityBAL
    {
        /// <summary>
        /// To get all capacity data from db
        /// </summary>
        /// <returns>List of capacity data</returns>
        Task<List<CapacityModelDTO>> GetCapacityData(string MID);

        /// <summary>
        /// To get all capacity data from db
        /// </summary>
        /// <returns>List of capacity data</returns>
        List<CapacityModelDTO> GetCapacity(string MID);

        /// <summary>
        /// Get the filtered capacity Data
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<CapacityModelDTO>> GetFilteredCapacityData(FilterModelDTO filter);

        /// <summary>
        /// Get Name based on MID
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        Task<string> GetNameBasedOnMID(string MID);

        /// <summary>
        /// Get competency of the user
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        Task<string> GetCompetencyBasedOnMID(string MID);

        /// <summary>
        /// Check if the ID exists in our table
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        Task<bool> IsValidEmployee(string MID);

        /// <summary>
        /// Method to get global search result
        /// </summary>
        /// <returns></returns>
        Task<List<CapacityModelDTO>> GetGlobalSearchResult(string key);
        Task<CapacityModelDTO> GetUserProfileDetails(string MID, string ConnectionString);

    }
}
