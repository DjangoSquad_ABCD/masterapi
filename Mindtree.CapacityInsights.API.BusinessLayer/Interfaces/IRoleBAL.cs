﻿/***************************************************************************************************************************
* Page Name  : IRoleBAL.cs
* Created By : Hanisha Hanumula 
* Created On : 5 Nov 2018
* Description : role BAL interface
* Modified By  :   
* Modified Date :   
* Modified Reason:  
***************************************************************************************************************************/

using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IRoleBAL
    {
        /// <summary>
        /// Save user's footprints to audit table
        /// <returns></returns>
        Task<Boolean> SaveAuditLog(AuditLogDTO auditLogDTO);

        /// <summary>
        /// Fetching the role for the specific MID
        /// <returns></returns>
        Task<int> GetRole(string MID);

        /// <summary>
        /// Fetching the all role 
        /// <returns></returns>
        Task<List<UserModelDTO>> GetAllAdmins();

        /// <summary>
        /// Create a role for specific MID
        /// <returns></returns>
        Task<Boolean> CreateRole(RoleModelDTO role);
        /// <summary>
        /// Delete a role for specific MID
        /// <returns></returns>
        Task<Boolean> DeleteRole(UserModelDTO role);

        Task<Boolean> VerifyAdminRole(string MidOfLoggedInUser);
    }
}


