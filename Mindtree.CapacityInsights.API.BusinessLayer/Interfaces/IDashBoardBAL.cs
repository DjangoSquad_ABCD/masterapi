﻿/***************************************************************************************************************************
* Page Name  : IDashBoardBAL.cs
* Created By : Madhushree Sv 
* Created On : 28 Nov 2018
* Description : Dashboard BAL interface
* Modified By  :  
* Modified Date :  
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IDashBoardBAL
    {

        /// <summary>
        /// This is controller post method to get Mindtree minds count
        /// </summary>
        /// <returns>Mindtree minds count</returns>
        Task<int> GetMindtreemindsCount();

        /// <summary>
        /// This is controller get method to get Unbilled minds count
        /// </summary>
        /// <returns>Unbilled minds</returns>
        Task<int> GetUnBilledMindsCount();

        /// <summary>
        /// This is controller get method to get deployable count
        /// </summary>
        /// <returns>deployable count</returns>
        Task<int> GetDeployableCount();

        /// <summary>
        /// This is controller get method to get Competency Building count
        /// </summary>
        /// <returns>Competency Building count</returns>
        Task<int> GetCompetencyBuildingCount();
        /// <summary>
        /// This is controller get method to get Billed minds count
        /// </summary>
        /// <returns>Billed minds count</returns>
        Task<int> GetBilledMindsCount();
        /// <summary>
        /// Get offshore onsite data
        /// </summary>
        /// <returns></returns>
        Task<List<int>> GetOnOffShoreCount();
        /// <summary>
        /// Get male female data
        /// </summary>
        /// <returns>Mindtree minds count</returns>
        Task<List<int>> GetMaleFemaleCount();

        /// <summary>
        /// Get engagemnet type data
        /// </summary>
        /// <returns>Engagemnet type  count</returns>
        Task<List<int>> GetEngagementTypeCount();

        /// <summary>
        /// Get number of people in delivery Manager
        /// </summary>
        /// <returns></returns>
        Task<List<DeliveryManagerCount>> GetDeliveryManager();

        /// <summary>
        /// get number of people vertical wise
        /// </summary>
        /// <returns></returns>
        Task<List<VerticalWiseCount>> GetVerticalCount();
    }
}
