﻿/*************************************************************************************************
* Page Name  : IThemeBAL.cs
* Created By : Supraja Harish
* Created On : 
* Description : IThemeBAL interface
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using Mindtree.CapacityInsights.API.DataLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.Interfaces
{
    public interface IThemeBAL
    {
        /// <summary>
        /// Save theme
        /// </summary>
        /// <param name="themeDTO">Theme DTO object</param>
        /// <returns></returns>
        Task<Boolean> SaveTheme(ThemeDTO themeDTO);

        /// <summary>
        /// Get theme saved by the user
        /// </summary>
        /// <param name="MID">MID of current user</param>
        /// <returns></returns>
        Task<ThemeDTO> GetTheme(string MID);
    }
}
