﻿/***************************************************************************************************************************
* Page Name  : RolBAL.cs
* Created By : Hanisha Hanumula 
* Created On : 5 Nov 2018
* Description : role BAL 
* Modified By  :   
* Modified Date :   
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.Repositories;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class RoleBAL : IRoleBAL
    {
        #region private variables 
        //readonly variable
        private readonly IRoleRepository _role;
        #endregion

        #region Constructor
        /// <summary>
        /// assign role value to a private value
        /// <returns></returns>

        public RoleBAL(IRoleRepository roleRepo)
        {
            this._role = roleRepo;
        }
        #endregion

        #region SaveAuditLog
        /// <summary>
        /// Save user's footprints
        /// <returns></returns>
        public async Task<Boolean> SaveAuditLog(AuditLogDTO auditLogDTO)
        {
            try
            {
                return await _role.SaveAuditLog(auditLogDTO);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region GetRole
        /// <summary>
        /// Fetching the role for the specific MID
        /// <returns></returns>
        public async Task<int> GetRole(string MID)
        {

            int role = await this._role.GetRole(MID);
            return role;
        }
        #endregion

        #region GetAllAdmins
        /// <summary>
        /// Fetching the all role 
        /// <returns></returns>
        public async Task<List<UserModelDTO>> GetAllAdmins()
        {
            return await this._role.GetAllAdmins();
        }
        #endregion

        #region CreateRole
        /// <summary>
        /// Create a role for specific MID
        /// <returns></returns>
        public async Task<Boolean> CreateRole(RoleModelDTO role)
        {
            return await this._role.CreateRole(role);
          
        
}
        #endregion

        #region DeleteRole
        /// <summary>
        /// Delete a role for specific MID
        /// <returns></returns>
        public async Task<Boolean> DeleteRole(UserModelDTO role)
        {
            return await this._role.DeleteRole(role);
        }
        #endregion

        #region verify admin role
        /// <summary>
        /// verify admin role
        /// </summary>
        /// <param name="MidOfLoggedInUser"></param>
        /// <returns></returns>
        public async Task<Boolean> VerifyAdminRole(string MidOfLoggedInUser)
        {
            return await this._role.VerifyAdminRole(MidOfLoggedInUser);
        }
        #endregion
    }
}