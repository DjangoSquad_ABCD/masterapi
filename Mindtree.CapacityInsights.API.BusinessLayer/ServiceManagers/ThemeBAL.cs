﻿/*************************************************************************************************
* Page Name  : ThemeBAL.cs
* Created By : Supraja Harish
* Created On : 
* Description : ThemeBAL class
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class ThemeBAL : IThemeBAL
    {
        #region private variables 
        //readonly variable for theme repository interface
        private readonly IThemeRepository _themeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// assign role value to a private value
        /// <returns></returns>

        public ThemeBAL(IThemeRepository themeRepository)
        {
            this._themeRepository = themeRepository;
        }
        #endregion

        #region SaveTheme
        /// <summary>
        /// Save user's theme
        /// </summary>
        /// <param name="themeDTO">Theme DTO object</param>
        /// <returns></returns>
        public async Task<Boolean> SaveTheme(ThemeDTO themeDTO)
        {
            try
            {
                //call to DAL layer method to save the Theme DTO object
                return await _themeRepository.SaveTheme(themeDTO);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region GetTheme
        /// <summary>
        /// Get theme saved by the user
        /// </summary>
        /// <param name="MID">MID of current user</param>
        /// <returns></returns>
        public async Task<ThemeDTO> GetTheme(string MID)
        {
            //call to DAL layer method to get the Theme DTO object for current user.
            return await _themeRepository.GetTheme(MID);
        }
        #endregion
    }
}
