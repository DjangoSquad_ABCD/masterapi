﻿/***************************************************************************************************************************
* Page Name  : AuditLogBAL.cs
* Created By : Supraja Harish  
* Created On : 10 Dec 2018
* Description : Method to call DAL layer method which saves logs contanining login details for auditing
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class AuditLogBAL : IAuditLogBAL
    {
        #region private variables 
        //readonly variable
        private readonly IAuditLogRepository _auditLogRepository;
        #endregion
        #region constructor
        /// <summary>
        /// constructor the auditLog Repository
        /// </summary>
        /// <param name="auditLogRepository"></param>
        public AuditLogBAL(IAuditLogRepository auditLogRepository)
        {
            this._auditLogRepository = auditLogRepository;
        }
        #endregion
        #region SaveLoginHistory
        /// <summary>
        /// Save Login History
        /// </summary>
        /// <param name="auditLogDTO"></param>
        /// <returns></returns>
        public async Task SaveLoginHistory(AuditLogDTO auditLogDTO)
        {
            await _auditLogRepository.SaveLoginHistory(auditLogDTO);
        }
        #endregion
    }
}
