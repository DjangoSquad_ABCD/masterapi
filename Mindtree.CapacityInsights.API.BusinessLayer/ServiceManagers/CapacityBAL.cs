﻿/***************************************************************************************************************************
* Page Name  : CapacityController.cs
* Created By : Rakshitha R  
* Created On : 10 Dec 2018
* Description : Capacity report related API's are coded in this controller
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class CapacityBAL: ICapacityBAL
    {
        #region private variables
        //private variable capacityReport
        private readonly ICapacityRepository _capacityRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// constructor for CapacityBAL
        /// </summary>
        /// <param name="capacityRepository"></param>
        public CapacityBAL(ICapacityRepository capacityRepository)
        {
            this._capacityRepository = capacityRepository;
        }

        #endregion

        #region GetCapacityData
        /// <summary>
        /// To get all capacity data from db
        /// </summary>
        /// <returns>List of capacity data</returns>
        public async Task<List<CapacityModelDTO>> GetCapacityData(string MID)
        {
            try
            {
                return await _capacityRepository.GetCapacityData(MID);
            }
            catch (Exception)
            {
                throw;
            }            
        }

        #endregion

        #region GetCapacity
        /// <summary>
        /// To get all capacity data from db
        /// </summary>
        /// <returns>List of capacity data</returns>
        public List<CapacityModelDTO> GetCapacity(string MID)
        {
            try
            {
                return _capacityRepository.GetCapacity(MID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetFilteredCapacityData
        /// <summary>
        /// Get Filtered Capacity Data
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<CapacityModelDTO>> GetFilteredCapacityData(FilterModelDTO filter)
        {
            try
            {
                return await _capacityRepository.GetFilteredCapacityData(filter);
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region GetCompetencyBasedOnMID
        /// <summary>
        /// Get competency of the user
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<string> GetCompetencyBasedOnMID(string MID)
        {
            try
            {
                return await _capacityRepository.GetCompetencyBasedOnMID(MID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetCompetencyBasedOnMID
        /// <summary>
        /// Get Name based on MID
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<string> GetNameBasedOnMID(string MID)
        {
            try
            {
                return await _capacityRepository.GetNameBasedOnMID(MID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region IsValidEmployee
        /// <summary>
        /// Check if the ID exists in the table.
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<bool> IsValidEmployee(string MID)
        {
            try
            {
                return await _capacityRepository.IsValidEmployee(MID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region GetGlobalSearchResult
        /// <summary>
        /// Refresh global search result based on the key entered. Return the capacity data that matches the entered key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<List<CapacityModelDTO>> GetGlobalSearchResult(string key)
        {
            try
            {
                return await this._capacityRepository.GetGlobalSearchResult(key);
            }
            catch (Exception)
            {

                throw;
            }
           

        }
        #endregion

        public async Task<CapacityModelDTO> GetUserProfileDetails(string MID, string ConnectionString)
        {
            try
            {
                return await this._capacityRepository.GetUserProfileDetails(MID,ConnectionString);
            }
            catch (Exception)
            {

                throw;
            }


        }

    }
}
