﻿/***************************************************************************************************************************
* Page Name  : PreferenceBAL.cs
* Created By : 
* Created On : 
* Description : Preference API BAL layer methods.
* Modified By  : Supraja Harish   
* Modified Date :   02/07/2019
* Modified Reason: Code comments
***************************************************************************************************************************/


using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class PreferenceBAL: IPreferenceBAL
         
    {
        #region private variables 
        /// <summary>
        /// Preference repository object instantiation
        /// </summary>
        private readonly IPreferenceRepository _prefrepo;
        #endregion

        #region Constructor
        public PreferenceBAL(IPreferenceRepository prefrepo)
        {
            this._prefrepo = prefrepo;
        }
        #endregion

        #region SavePreferences
        /// <summary>
        /// Call to dal layer to save preferences
        /// </summary>
        /// <param name="preferencesModelDTO">Preference model DTO object</param>
        /// <returns></returns>
        public async Task<bool> SavePreferences(PreferencesModelDTO preferencesModelDTO)
        {
            try
            {
                //DAL layer call to save prefernce
                return await _prefrepo.SavePreferences(preferencesModelDTO);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetTblProfilePreference
        /// <summary>
        /// Get all preferences for the MID passed.
        /// </summary>
        /// <param name="MID">MID of the current user</param>
        /// <returns>List of preferences saved by the user</returns>
        public async Task<List<PreferencesModelDTO>> GetTblProfilePreference(string MID)
        {
            try
            {
                //DAL layer call to get all prefernces
                return await _prefrepo.GetTblProfilePreference(MID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetDefaultPreference
        /// <summary>
        /// To get default preference if any 
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<PreferencesModelDTO> GetDefaultPreference(string MID)
        {
            try
            {
                //DAL layer call to get default prefernce.
                return await _prefrepo.GetDefaultPreference(MID);
                 
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region GetDefaultPreferenceForDisplay
        /// <summary>
        /// To get default preference if any or return null
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        public async Task<PreferencesModelDTO> GetDefaultPreferenceForDisplay(string MID)
        {
            try
            {
                //DAL layer call to get name of default prefernce
                return await _prefrepo.GetDefaultPreferenceForDisplay(MID);

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region ChangeDefaultPreference
        /// <summary>
        /// Update default preference of a user.
        /// </summary>
        /// <param name="preference">Preference model DTO object.</param>
        /// <returns></returns>
        public async  Task ChangeDefaultPreference(PreferencesModelDTO preference)
        {
            try
            {
                //DAL layer call to update default prefernces
                await this._prefrepo.UpdateDefaultPreference(preference);
               

            }

            catch (Exception)
            {

                throw;
            }      
        }
        #endregion

    }
}
