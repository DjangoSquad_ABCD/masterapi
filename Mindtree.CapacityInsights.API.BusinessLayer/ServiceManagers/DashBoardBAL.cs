﻿/***************************************************************************************************************************
* Page Name  : DashBoardBAL.cs
* Created By : Madhushree Sv 
* Created On : 28 Nov 2018
* Description : Dashboard BAL 
* Modified By  :  
* Modified Date :  
* Modified Reason:  
***************************************************************************************************************************/
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class DashBoardBAL : IDashBoardBAL
    {
        #region private variables 
        private readonly IDashBoardRepository _dashboardReporitory;
        #endregion

        #region constructor
        public DashBoardBAL(IDashBoardRepository dashboardReporitory)
        {
            this._dashboardReporitory = dashboardReporitory;
        }
        #endregion

        #region GetUnBilledMindsCount
        /// <summary>
        /// This is controller post method to get Unbilled minds count
        /// </summary>
        /// <returns>Unbilled minds count</returns>
        public async Task<int> GetUnBilledMindsCount()
        {
            try
            {
                return await this._dashboardReporitory.GetUnBilledMindsCount();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetCompetencyBuildingCount
        /// <summary>
        /// This is controller get method to get Competency building count
        /// </summary>
        /// <returns>Competency building</returns>

        public async Task<int> GetCompetencyBuildingCount()
        {
            try
            {
                return await this._dashboardReporitory.GetCompetencyBuildingCount();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetBilledMindsCount
        /// <summary>
        /// This is controller get method to get Billed minds count
        /// </summary>
        /// <returns>Billed minds count</returns>

        public async Task<int> GetBilledMindsCount()
        {
            try
            {
                return await this._dashboardReporitory.GetBilledMindsCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetDeployableCount
        /// <summary>
        /// This is controller get method to get deployable count
        /// </summary>
        /// <returns>deployable count</returns>
        public async Task<int> GetDeployableCount()
        {
            try
            {
                return await this._dashboardReporitory.GetDeployableCount();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetMindtreemindsCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count
        /// </summary>
        /// <returns>Mindtree minds count</returns>
        public async Task<int> GetMindtreemindsCount()
        {
            try
            {
                return await this._dashboardReporitory.GetMindtreemindsCount();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetOnOffShoreCount
        /// <summary>
        /// Get the onsite offshore counts
        /// </summary>
        /// <returns></returns>
        public async Task<List<int>> GetOnOffShoreCount()
        {
            try
            {
                return await this._dashboardReporitory.GetOnOffShoreCount(); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region GetMaleFemaleCount
        /// <summary>
        /// Get the male female counts
        /// </summary>
        /// <returns></returns>
        public async Task<List<int>> GetMaleFemaleCount()
        {
            try
            {
                return await this._dashboardReporitory.GetMaleFemaleCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetEngagementTypeCount
        /// <summary>
        /// Get the engagement type counts
        /// </summary>
        /// <returns></returns>
        public async Task<List<int>> GetEngagementTypeCount()
        {
            try
            {
                return await this._dashboardReporitory.GetEngagementTypeCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetDeliveryManager
        /// <summary>
        /// Get the number of people under a specific delivery manager
        /// </summary>
        /// <returns></returns>
        public async Task<List<DeliveryManagerCount>> GetDeliveryManager()
        {
            try
            {
               return await this._dashboardReporitory.GetDeliveryManager();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetVerticalCount
        /// <summary>
        /// get the people vertical wise
        /// </summary>
        /// <returns></returns>
        public async Task<List<VerticalWiseCount>> GetVerticalCount()
        {
            try
            {
                return await this._dashboardReporitory.GetVerticalCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

    }
}
