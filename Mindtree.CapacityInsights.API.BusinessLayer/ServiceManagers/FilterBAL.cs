﻿/***************************************************************************************************************************
* Page Name  : FilterBAL.cs
* Created By : Rakshitha R  
* Created On : 1 Dec 2018
* Description : Methods to get Filtered data
* Modified By  :  Supraja Harish, Ayushi Khandelwal
* Modified Date :  18 Jan 2019
* Modified Reason: Modified existing methods
***************************************************************************************************************************/

using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers
{
    public class FilterBAL : IFilterBAL
    {
        #region private variables
        //readonly variables
        private readonly IFilterRepository _filter;

        #endregion


        #region constructor
        //constructor
        public FilterBAL(IFilterRepository filter)
        {
            this._filter = filter;
        }
        #endregion


        #region GetAllFirstFilterOptions
        /// <summary>
        /// Fetching all the DeliveryPartners,DeliveryManagers,ProfitCenters,ProjectNames,AccountGroups,Practices
        /// <returns></returns> 
        public async Task<FilterModelDTO> GetAllFirstFilterOptions()
        {
            try
            {
                // call DAL layer to get all first filters
                return await this._filter.GetAllFirstFilterOptions();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region GetAllSecondFilterOptions
        /// <summary>
        /// Fetching all the BillableStatuses,Locations,SRStatuses, EngagemeentTypes
        /// <returns></returns>   
        public async Task<FilterModelDTO> GetAllSecondFilterOptions()
        {
            try
            {
                //call DAL layer to get all second filter options
                return await this._filter.GetAllSecondFilterOptions();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region GetInterrelatedFilters
        /// <summary>
        /// get all the data using filtered profit center
        /// </summary>
        /// <param name="interrelatedFilter"></param>
        /// <returns></returns>
        public async Task<FilterModelDTO> GetInterrelatedFilters(FilterModelDTO interRelatedFilter)
        {
            try
            {
                // to get all interrelatedfilters based on filters applied
                return await this._filter.GetInterrelatedFilters(interRelatedFilter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion


    }
}
