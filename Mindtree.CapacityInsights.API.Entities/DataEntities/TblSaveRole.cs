﻿/*************************************************************************************************
* Page Name  : TblSaveRole.cs
* Created By : Supraja Harish
* Created On : 
* Description : Properties in SaveRole table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public class TblSaveRole
    {
        // These are the attributes or column names in TblSaveRole table
        public Guid Id { get; set; }
        public string MID { get; set; }
        public int RID { get; set; }
        public int ActiveInactive { get; set; }
        public DateTime UploadedDate { get; set; }
        public string Uploadedby { get; set; }
    }
}

