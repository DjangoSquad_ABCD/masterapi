﻿/*************************************************************************************************
* Page Name  : TblRole.cs
* Created By : Supraja Harish
* Created On : 
* Description : Properties in Role table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public class TblRole
    {
        // These are the attributes or column names in TblRole table
        public int RId { get; set; }
        public string GivenRole { get; set; }
        public string RoleDescription { get; set; }

    }
}