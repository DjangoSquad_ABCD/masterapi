﻿/*************************************************************************************************
* Page Name  : TblProfilePrefrence.cs
* Created By : 
* Created On : 
* Description : Properties in ProfilePrefrence table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public partial class TblProfilePrefrence
    {
        // These are the attributes or column names in TblProfilePrefrence table
        public string Mid { get; set; }
        public bool DefaultPreference { get; set; }
        public string FilterName { get; set; }
        public string Preferences { get; set; }
        public DateTime UploadedDate { get; set; }
        public string Uploadedby { get; set; }
    }
}
