﻿/*************************************************************************************************
* Page Name  : TblAuditLog.cs
* Created By : Sujit
* Created On : 
* Description : Properties in AuditLog table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public partial class TblAuditLog
    {
        // These are the attributes or column names in tblAuditLog table
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string UserAction { get; set; }
        public string ReqId { get; set; }
        public string Reference { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public DateTime? UploadedDateTime { get; set; }
        public string UploadedBy { get; set; }
        public string UploadedFor { get; set; }
    }
}
