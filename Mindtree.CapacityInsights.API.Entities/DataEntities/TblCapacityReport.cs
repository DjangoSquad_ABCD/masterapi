﻿/*************************************************************************************************
* Page Name  : TblCapacityReport.cs
* Created By : Sujit
* Created On : 
* Description : Properties in CapacityReport table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public partial class TblCapacityReport
    {
        // These are the attributes or column names in TblCapacityReport table
        public string EmployeeId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Competency { get; set; }
        public string GivenRole { get; set; }
        public string Segment { get; set; }
        public string Location { get; set; }
        public string OnOffShore { get; set; }
        public string EmpStatus { get; set; }
        public string Billability { get; set; }
        public string Vertical { get; set; }
        public string SubVertical { get; set; }
        public string Practice { get; set; }
        public string SubPractice { get; set; }
        public string CustomerName { get; set; }
        public string ProfitCenter { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string EngagementType { get; set; }
        public string ProjectType { get; set; }
        public string ProjectRole { get; set; }
        public string ProjectManagerRequestor { get; set; }
        public string DeliveryManager { get; set; }
        public string DeliveryPartner { get; set; }
        public string IgdeliveryHead { get; set; }
        public string BillableStatus { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Srstatus { get; set; }
        public string StaffingRequestId { get; set; }
        public string ReasonForBlocking { get; set; }
        public string ReasonForUnbillable { get; set; }
        public string UnbillableDays { get; set; }
        public string Remarks { get; set; }
        public string CampusTag { get; set; }
        public double? BillablePercentageStaffed { get; set; }
        public double? UnbillablePercentageStaffed { get; set; }
        public double? TotalPercentageStaffed { get; set; }
        public string Technology { get; set; }
        public string DateOfJoining { get; set; }
        public string GivenFunction { get; set; }
        public string AccountGroup { get; set; }
        public string VisaCategory { get; set; }
        public string SubCategory { get; set; }
        public string GivenType { get; set; }
        public string ExpiryDate { get; set; }
        public string StampingStatus { get; set; }
        public string Comments { get; set; }
        public string RequiredYear { get; set; }
        public string Ijpstuatus { get; set; }
        public string IjpappliedSrid { get; set; }
        public string IjpappliedDate { get; set; }
        public string WorkLocation { get; set; }
        public string Klocation { get; set; }
        public string CustomerGroup { get; set; }
        public double? BilledF { get; set; }
        public int? UnbilledF { get; set; }
        public int? BlockedF { get; set; }
        public int? InternalPrjInvestmentF { get; set; }
        public int? InternalPrjCostF { get; set; }
        public int? AvailableF { get; set; }
        public int? NonDeployF { get; set; }
        public int? TrainingF { get; set; }
        public int? LongLeaveF { get; set; }
        public int? SalesF { get; set; }
        public int? SupportF { get; set; }
        public string PeopleOwner { get; set; }
        public string AccountCategory { get; set; }
        public string PrimaryL1 { get; set; }
        public string PrimaryL2 { get; set; }
        public string PrimaryL3 { get; set; }
        public string PrimaryL4 { get; set; }
        public string SecondaryL1 { get; set; }
        public string SecondaryL2 { get; set; }
        public string SecondaryL3 { get; set; }
        public string SecondaryL4 { get; set; }
        public string TertiaryL1 { get; set; }
        public string TertiaryL2 { get; set; }
        public string TertiaryL3 { get; set; }
        public string TertiaryL4 { get; set; }
        public string Rtpf { get; set; }
        public string Resigned { get; set; }
        public string CurrentWorkLocation { get; set; }
        public string PreferredWorkLocation { get; set; }
        public string LastStaffingEndDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string LastUpdatedOn { get; set; }
        public bool? IsDataCorrect { get; set; }
        public string ConfirmedOn { get; set; }
        public string Yorbit101 { get; set; }
        public string Yorbit201 { get; set; }
        public string Yorbit301 { get; set; }
        public string TotalBlockedDays { get; set; }
        public int? TotalExperience { get; set; }
    }
}
