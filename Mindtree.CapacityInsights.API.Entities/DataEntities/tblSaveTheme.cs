﻿/*************************************************************************************************
* Page Name  : TblSaveTheme.cs
* Created By : Supraja Harish
* Created On : 
* Description : Properties in SaveTheme table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public partial class TblSaveTheme
    {
        public string MID { get; set; }
        public string Theme { get; set; }
    }
}
