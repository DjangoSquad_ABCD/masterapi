﻿/*************************************************************************************************
* Page Name  : TblExceptionLog.cs
* Created By : Sujit
* Created On : 
* Description : Properties in ExceptionLog table
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/

using System;
using System.Collections.Generic;

namespace Mindtree.CapacityInsights.API.Entities.DataEntities
{
    public partial class TblExceptionLog
    {
        // These are the attributes or column names in TblExceptionLog table
        public int Id { get; set; }
        public string ExceptionLevel { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string TargetSite { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
