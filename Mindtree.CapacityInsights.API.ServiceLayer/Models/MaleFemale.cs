﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Models
{
    public class MaleFemale
    {
        public int Male { get; set; }
        public int Female { get; set; }
    }
}
