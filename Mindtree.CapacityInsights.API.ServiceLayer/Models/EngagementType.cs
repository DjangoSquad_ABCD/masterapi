﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Models
{
    public class EngagementType
    {
        public int notListed { get; set; }
        public int HYB { get; set; }
        public int TNM { get; set; }
        public int FMC { get; set; }
        public int FPC { get; set; }


    }
}
