﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Models
{
    public class OnOffshore
    {
        /// <summary>
        /// store offshore counts
        /// </summary>
        public int offshore { get; set; }

        /// <summary>
        /// store onsite counts
        /// </summary>
        public int onsite { get; set; }

        /// <summary>
        /// store other counts
        /// </summary>
        public int not_listed { get; set; }
    }
}
