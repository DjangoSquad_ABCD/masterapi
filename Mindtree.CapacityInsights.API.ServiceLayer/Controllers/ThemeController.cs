﻿/*************************************************************************************************
* Page Name  : ThemeController.cs
* Created By : Supraja Harish
* Created On : 
* Description : ThemeController class
* Modified By  :  
* Modified Date :  
* Modified Reason: 
**************************************************************************************************/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThemeController : ControllerBase
    {
        #region private vaariables
        private readonly IThemeBAL _themeBAL; // Private variable to access methods in ICapacityBAL
        #endregion

        #region Constructor
        public ThemeController(IThemeBAL themeBAL)
        {
            this._themeBAL = themeBAL;
        }
        #endregion

        #region SaveTheme
        /// <summary>
        /// Save theme selected by the user
        /// </summary>
        /// <param name="themeDTO">Theme DTO object</param>
        /// <returns>status code</returns>
        [HttpPost("SaveTheme")]
        public async Task<IActionResult> SaveTheme([FromBody]ThemeDTO themeDTO)
        {
            try {
                if(await _themeBAL.SaveTheme(themeDTO))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch {
                return NotFound();
            }
        }
        #endregion

        #region GetTheme
        /// <summary>
        /// Get theme saved by the user
        /// </summary>
        /// <param name="MID">MID of the current user</param>
        /// <returns>Theme DTO object</returns>
        [HttpGet("GetTheme")]
        public async Task<IActionResult> GetTheme(string MID)
        {
            try
            {
                //Call to BAL method to get the theme details of the current user.
                return Ok(await _themeBAL.GetTheme(MID));
            }
            catch
            {
                return NotFound();
            }
        }
            #endregion

        }
}