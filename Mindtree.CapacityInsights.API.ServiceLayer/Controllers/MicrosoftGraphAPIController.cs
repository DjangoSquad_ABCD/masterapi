﻿/***************************************************************************************************************************
* Page Name  : MicrosoftGraphAPIController.cs
* Created By : Adabala Sujit
* Created On : 28 Dec 2018
* Description : Method to fetch user details
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;
using Mindtree.CapacityInsights.API.Utilities;
using Dapper;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.IO;
using System.Json;
using System.Buffers.Text;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using System.Data.SqlClient;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MicrosoftGraphAPIController : ControllerBase
    {
        #region private variables 
        private readonly IAuditLogBAL _auditLogBAL; // private variable to access methods in                                                   IAuditLogBAL
        private readonly ICapacityBAL _capacityBAL; // private variable to access methods in                                                   ICapacityBAL
        private readonly IRoleBAL _roleBAL;         // private variable to access methods in                                                   IRoleBAL
        private readonly IDataEncryptionDecryption _dataEncryptionDecryption;
        private readonly IHostingEnvironment _env;
        #endregion

        #region public variables 
        public IConfiguration Configuration { get; }
        public object CryptoJS { get; private set; }
        #endregion

        #region constructor
        public MicrosoftGraphAPIController(IAuditLogBAL auditLogBAL, ICapacityBAL capacityBAL, IConfiguration Configuration, IRoleBAL roleBAL, IDataEncryptionDecryption dataEncryptionDecryption, IHostingEnvironment env)
        {
            this._auditLogBAL = auditLogBAL;
            this._capacityBAL = capacityBAL;
            this._roleBAL = roleBAL;
            this.Configuration = Configuration;
            this._dataEncryptionDecryption = dataEncryptionDecryption;
            this._env = env;
        }
        #endregion

        #region GetUserDetails
        /// <summary>
        /// Get user details based on the ID passed
        /// </summary>
        /// <param name="EncryptedMID"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetUserDetails")]
        public async Task<IActionResult> GetUserDetails(string EncryptedMID)
        {
            try
            {
                return StatusCode(400);
                var base64EncodedBytes = System.Convert.FromBase64String(EncryptedMID);
                var encryptedMID = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                byte[] encryptText = Convert.FromBase64String(encryptedMID);
                var ID = await _dataEncryptionDecryption.Decryption(encryptText);
                string encryptedData = "";

                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(ID) || startsWithM.IsMatch(ID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    //Check if user ID is present in the capacity table, if yes then proceed further.
                    if (await _capacityBAL.IsValidEmployee(ID))
                    {
                        int roleID = await _roleBAL.GetRole(ID);
                        //Generate JWT
                        var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
                        var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SecretKey"]));
                        var signingCreds = new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256);
                        var encryptingCreds = new EncryptingCredentials(secret, SecurityAlgorithms.Aes128KW, SecurityAlgorithms.Aes128CbcHmacSha256);
                        var handler = new JwtSecurityTokenHandler();
                        var token = handler.CreateJwtSecurityToken(
                       "Ozone",
                       "Ozone",
                       new System.Security.Claims.ClaimsIdentity(),
                       DateTime.Now,
                       DateTime.Now.AddMinutes(15),
                       DateTime.Now,
                       signingCreds,
                       encryptingCreds);
                        token.Payload["MID"] = ID;
                        token.Payload["RoleID"] = roleID;
                        var jwt = new JwtSecurityTokenHandler().WriteToken(token);
                        //check if ID starts with M or not, if not then append M in the beginning.
                        if (!ID.StartsWith('M'))
                        {
                            ID = "M" + ID;
                        }

                        //Get all user related details from capacity table.
                        string name = await _capacityBAL.GetNameBasedOnMID(ID);
                        int roleOfUser = await _roleBAL.GetRole(ID);
                        UserModelDTO userDetails = new UserModelDTO();
                        userDetails.EmployeeID = ID;
                        userDetails.Competency = await _capacityBAL.GetCompetencyBasedOnMID(ID);
                        userDetails.Name = name;
                        userDetails.Token = jwt;
                        userDetails.Role = roleOfUser;
                        string userDetailsData = userDetails.Competency + "," + userDetails.EmployeeID + "," + userDetails.Name + "," + userDetails.Token + "," + userDetails.Role;
                        encryptedData = await _dataEncryptionDecryption.Encryption(userDetailsData);
                        //Record users footprints to Audit table
                        AuditLogDTO auditLogDTO = new AuditLogDTO();
                        auditLogDTO.EmployeeID = ID;
                        auditLogDTO.EmployeeName = name;
                        auditLogDTO.UserAction = GlobalConstants.Constants.USER_ACTION_LOGGED_IN;
                        await _roleBAL.SaveAuditLog(auditLogDTO);
                        return Ok(encryptedData);
                    }
                    else
                    {
                        return StatusCode(501);
                    }
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        #endregion

        /// <summary>
        /// Get user details
        /// </summary>
        /// <returns></returns>
        //[HttpGet]

        //public async Task<ActionResult> GetUserDetail()
        //{
        //    try
        //    {
        //        var queryString = HttpUtility.ParseQueryString(string.Empty);
        //        queryString["api-version"] = "1.6";
        //        var url = "https://graph.microsoft.com/v1.0/me/" + queryString;

        //        var request = new HttpRequestMessage(HttpMethod.Get,
        //        url);
        //        //request.Headers.Add("Accept", "application/vnd.github.v3+json");
        //        //request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

        //        //var client = _clientFactory.CreateClient();

        //        //var response = await client.SendAsync(request);

        //        //if (response.IsSuccessStatusCode)
        //        //{
        //        //    Branches = await response.Content
        //        //        .ReadAsAsync<IEnumerable<GitHubBranch>>();
        //        //}
        //        //else
        //        //{
        //        //    GetBranchesError = true;
        //        //    Branches = Array.Empty<GitHubBranch>();
        //        //}
        //        return null;
        //    }
        //    catch (Exception)
        //    {

        //        return NotFound();
        //    }


        #region GetUserProfileDetails
        /// <summary>
        /// Get user details based on the MID passed
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetProfileDetails")]
        public async Task<IActionResult> GetProfileDetails(string EncryptedMID)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(EncryptedMID);
            var encryptedMID = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            byte[] encryptText = Convert.FromBase64String(encryptedMID);
            var MID = await _dataEncryptionDecryption.Decryption(encryptText);
            string encryptedData = "";

            if (MID == null)
                return StatusCode(404);
            try
            {
                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithMorm = new Regex("^[Mm]{1}\\d{7}");

                if (startsWithMorm.IsMatch(MID))
                {
                    MID = MID.Substring(1, 7);
                }
                var profileDetails = new CapacityModelDTO();
                    var ConnectionString = "";
                    if (_env.IsDevelopment())
                    {
                        // it is the connection string used in development phase or local
                        ConnectionString = Configuration["sqlserver:ConnectionStringDev"];
                    }
                    else
                    {
                        //it is the connection string used in production phase
                        ConnectionString = Configuration["sqlserver:ConnectionStringProd"];
                    }
                    profileDetails = await _capacityBAL.GetUserProfileDetails(MID, ConnectionString);
                if(profileDetails.ProjectName==null)
                {
                    profileDetails.ProjectName = "Null";
                }
                else
                    if (profileDetails.Location == null)
                {
                    profileDetails.Location = "Null";
                }
                else
                    if (profileDetails.GivenRole == null)
                {
                    profileDetails.GivenRole = "Null";
                }
                else
                    if (profileDetails.BillableStatus == null)
                {
                    profileDetails.BillableStatus = "Null";
                }
                else
                    if (profileDetails.DeliveryManager == null)
                {
                    profileDetails.DeliveryManager = "Null";
                }
                else
                    if (profileDetails.Vertical == null)
                {
                    profileDetails.Vertical = "Null";
                }
                else
                    if (profileDetails.Title == null)
                {
                    profileDetails.Title = "Null";
                }
                    string userProfileData = profileDetails.ProjectName + "@" + profileDetails.Location + "@" + 
                                            profileDetails.GivenRole + "@" + profileDetails.BillableStatus  + "@" + 
                                            profileDetails.DeliveryManager + "@" + profileDetails.Vertical + "@" + 
                                            profileDetails.Title;
                encryptedData = await _dataEncryptionDecryption.Encryption(userProfileData);

                return Ok(encryptedData);
            }
            catch(Exception )
            {
                return StatusCode(500);
            }
        }


    }
}
    #endregion