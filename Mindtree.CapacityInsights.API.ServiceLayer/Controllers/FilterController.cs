﻿/***************************************************************************************************************************
* Page Name  : FilterController.cs
* Created By : Rakshitha R  
* Created On : 1 Dec 2018
* Description : Methods to get Filtered data
* Modified By  :  Supraja Harish, Ayushi Khandelwal
* Modified Date :  18 Jan 2019
* Modified Reason: Modified existing methods
***************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Newtonsoft.Json;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilterController : Controller
    {
        #region private variables
        //read only variables
        private readonly IRoleBAL _rolBAL;
        private readonly ICapacityBAL _capacityBAL;
        private readonly IPreferenceBAL _prefBAL;
        private readonly IFilterBAL _filBAL;
        #endregion

        #region constructor
        //constructor of FilterController
        public FilterController(IRoleBAL rolBAL, ICapacityBAL capacityBAL, IFilterBAL filBAL, IPreferenceBAL _prefBAL)
        {
                this._rolBAL = rolBAL;
                this._capacityBAL = capacityBAL;
                this._filBAL = filBAL;
                this._prefBAL = _prefBAL;
        }

        #endregion

        #region Get filter preference
        /// <summary>
        /// gets the filter preference
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="FilterName"></param>
        /// <returns></returns>
        [HttpGet("GetFilterPreference")]
        public async Task<IActionResult> GetFilterPreference(string mid, string FilterName)
        {
            try
            {
                //call BAL method to get profile preferences based on mid
                var resultProfilePreferences = await _prefBAL.GetTblProfilePreference(mid);
                return Json(resultProfilePreferences.Where(x => x.FilterName == FilterName).Select(x => x.Preferences));
            }
            catch (Exception)
            {
                //not found 404 status code is sent on occurance of exception in any layer
                return NotFound();
            }    
        }
        #endregion

        #region Get Filters
        /// <summary>
        /// get all the filters of a mid
        /// </summary>
        /// <param name="mid"></param>
        /// <returns></returns>
        [HttpGet("GetFilters")]
        public async Task<IActionResult> GetFilters(string ID)
        {
            try
            {
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(ID) || startsWithM.IsMatch(ID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    //call BAL method to get filters based on mid
                    var profilePreference = await _prefBAL.GetTblProfilePreference(ID);
                    return Json(_prefBAL.GetTblProfilePreference(ID));
                }
                else
                {
                    //not found 404 status code is sent on occurance of exception in any layer
                    return NotFound();
                }
            }

            catch (Exception)
            {
                //not found 404 status code is sent on occurance of exception in any layer
                return NotFound();
            }   
        }
        #endregion
  
        #region Get Filter Options From DB


        /// <summary>
        /// Fetching all the DeliveryPartners,DeliveryManagers,ProfitCenters,ProjectNames,AccountGroups,Practices
        /// <returns></returns> 
        [HttpGet("GetAllFirstFilterOptions")]
        public async  Task<IActionResult> GetAllFirstFilterOptions()
        {
            try
            {
                //call BAL method to get first filter preferences based on mid
                return Json(await this._filBAL.GetAllFirstFilterOptions());
            }
            catch (Exception)
            {
                //not found 404 status code is sent on occurance of exception in any layer
                return NotFound();
            }

        }


        /// <summary>
        /// Fetching all the BillableStatuses, Locations, SRStatuses, EngagemeentTypes
        /// <returns></returns>   
        [HttpGet("GetAllSecondFilterOptions")]
        public async Task<IActionResult> GetAllSecondFilterOptions()
        {
            try
            {
                return Json( await this._filBAL.GetAllSecondFilterOptions());
            }
            catch (Exception)
            {

                return NotFound();
            }
            
        }

        #endregion

        #region Methods to get filtered filters
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interrelatedFilter"></param>
        /// <returns></returns>
        [HttpPost("GetInterrelatedFilters")]
        public async Task<IActionResult> GetInterrelatedFilters(FilterModelDTO interrelatedFilter)
        {
            try
            {
                return Ok( await this._filBAL.GetInterrelatedFilters(interrelatedFilter));
            }
            catch (Exception)
            {

                return NotFound();
            }        
        }
        #endregion
    }
}