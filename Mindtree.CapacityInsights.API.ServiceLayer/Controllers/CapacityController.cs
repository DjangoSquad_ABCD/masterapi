﻿/***************************************************************************************************************************
* Page Name  : CapacityController.cs
* Created By : Rakshitha R  
* Created On : 10 Dec 2018
* Description : Capacity report related API's are coded in this controller
* Modified By  :  
* Modified Date :  
* Modified Reason: 
***************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CapacityController : ControllerBase
    {
        #region private vaariables
        private readonly ICapacityBAL _capacityBAL; // Private variable to access methods in ICapacityBAL
        #endregion

        #region Constructor
        public CapacityController(ICapacityBAL capacityBAL)
        {
            this._capacityBAL = capacityBAL;
        }
        #endregion

        #region GetAllCapacityData
        /// <summary>
        /// Method to get all capacity data
        /// </summary>
        /// <returns>Capacity data from Database in json format</returns>
        [HttpGet]
        [ActionName("GetAllCapacityData")] 
        public async Task<IActionResult> GetAllCapacityData(string MID)
        {
            try
            {
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    //call BAL method to get capacity data
                    var capacityReport = await _capacityBAL.GetCapacityData(MID);
                    return Ok(capacityReport);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }            
        }
        #endregion

        #region GetCapacity
        /// <summary>
        /// Method to get all capacity data
        /// </summary>
        /// <returns>Capacity data from Database in json format</returns>
        [HttpGet]
        [ActionName("GetCapacity")]
        public IActionResult GetCapacity(string MID)
        {
            try
            {
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    var capacityReport = _capacityBAL.GetCapacity(MID);
                    return Ok(capacityReport);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion

        #region FilterCapacityData
        /// <summary>
        /// Method to get all capacity data
        /// </summary>
        /// <returns>Capacity data from Database in json format</returns>
        [HttpPost]
        [ActionName("FilteredData")]
        public async Task<IActionResult> FilterCapacityData([FromBody]FilterModelDTO filterData)
        {
            try
            {
                //call BAL method to get capacity data based on the applied filter
                var capacityReport = await _capacityBAL.GetFilteredCapacityData(filterData);
                return Ok(capacityReport);
            }
            catch (Exception)
            {
                //not found 404 status code is sent on occurance of exception in any layer
                return NotFound();
            }
        }
        #endregion

        #region GetGlobalSearchResult
        /// <summary>
        /// Get capacity data that matches with the key sent
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetGlobalSearchResult")]
        public async  Task<IActionResult> GetGlobalSearchResult(string key)
        {
            try
            {
                //call BAL method to get capacity data based on the key
                var result = await this._capacityBAL.GetGlobalSearchResult(key);
                return Ok(result);
            }
            catch (Exception ex)
            {
                //status code 500 will be returned on occurance of exception in any layer
                return StatusCode(500);
            }
          
        }
        #endregion

    }
}