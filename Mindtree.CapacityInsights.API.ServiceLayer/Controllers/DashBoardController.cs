﻿/***************************************************************************************************************************
* Page Name  : DashBoardController.cs
* Created By : Madhushree Sv 
* Created On : 28 Nov 2018
* Description : Dashboard controller
* Modified By  :  Ayushi Khandelwal
* Modified Date :   3 Jan 2019
* Modified Reason:  Bug fix
***************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.ServiceLayer.Models;
using Mindtree.CapacityInsights.API.Utilities;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DashBoardController : ControllerBase
    {
        #region private variables 
        private readonly IDashBoardBAL _dashBAL;
        #endregion
        #region constructor 
        public DashBoardController(IDashBoardBAL dashBAL)
        {
            this._dashBAL = dashBAL;
        }
        #endregion

        #region GetMindtreemindsCount
        /// <summary>
        /// This is controller get method to get Mindtree minds count
        /// </summary>
        /// <returns>Mindtree minds count</returns
        [HttpGet]
        [ActionName("GetMindtreemindsCount")]
        public async Task<IActionResult> GetMindtreemindsCount()
        {
            try
            {
                //call BAL method to get total mindtree mind's count
                return Ok(await this._dashBAL.GetMindtreemindsCount());
            }
            catch(Exception ex)
            {
                return NotFound();
            }
        }

        #endregion

        #region GetUnBilledMindsCount
        /// <summary>
        /// This is controller get method to get unbilled minds count
        /// </summary>
        /// <returns>Unbilled minds count</returns>
        [HttpGet]
        [ActionName("GetUnBilledMindsCount")]
        public async Task<IActionResult> GetUnBilledMindsCount()
        {
            try
            {
                //call BAL method to get total mindtree unbilled mind's count
                return Ok(await this._dashBAL.GetUnBilledMindsCount());
            }
            catch(Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
        #region GetDeployableCount
        /// <summary>
        /// This is controller get method to get deployable count
        /// </summary>
        /// <returns>deployable count</returns>
        [HttpGet]
        [ActionName("GetDeployableCount")]
        public async Task<IActionResult> GetDeployableCount()
        {
            try
            {
                //call BAL method to get total mindtree deployable mind's count
                return Ok(await this._dashBAL.GetDeployableCount());
            }
            catch(Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
        #region GetCompetencyBuildingCount
        ///<summary>
        ///This is controller get method to get Competency building count
        ///</summary>
        ///<returns>Competency building</returns>
        [HttpGet]
        [ActionName("GetCompetencyBuildingCount")]
        public async Task<IActionResult> GetCompetencyBuildingCount()
        {
            try
            {
                //call BAL method to get total mindtree competency building mind's count
                return Ok(await this._dashBAL.GetCompetencyBuildingCount());
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
        #region GetBilledMindsCount
        /// <summary>
        /// This is controller get method to get Billed Minds count
        /// </summary>
        /// <returns>Billed Minds Count</returns>

        [HttpGet]
        [ActionName("GetBilledMindsCount")]
        public async Task<IActionResult> GetBilledMindsCount()
        {
            try
            {
                //call BAL method to get total mindtree billed mind's count
                return Ok(await this._dashBAL.GetBilledMindsCount());
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
        #region GetOnOffShoreCount
        /// <summary>
        /// To get the OnsiteOffshore Data
        /// </summary>
        /// <returns>Competency building</returns>
        [HttpGet]
        [ActionName("GetOnOffShoreCount")]
        public async Task<IActionResult> GetOnOffShoreCount()
        {
            try
            {
                //variables
                List<int> listOnOffShoreCount = new List<int>();
                int i = 0;
                OnOffshore onOffshoreObj = new OnOffshore();

                //call BAL method to get Onshore Offshore mindtree minds count 
                listOnOffShoreCount = await this._dashBAL.GetOnOffShoreCount();

                //Map into model object
                foreach (var item in listOnOffShoreCount)
                {
                    if (i == GlobalConstants.Constants.INTEGER_ONE)
                        onOffshoreObj.onsite = item;
                    else if(i== GlobalConstants.Constants.INTEGER_TWO)        
                        onOffshoreObj.offshore = item;
                    else if (i == GlobalConstants.Constants.INTEGER_THREE)
                        onOffshoreObj.not_listed = item;
                    ++i;
                }
                return Ok(onOffshoreObj);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
            
        }
        #endregion
        #region GetMaleFemaleCount
        [HttpGet]
        [ActionName("GetMaleFemaleCount")]
        public async Task<IActionResult> GetMaleFemaleCount()
        {
            try
            {
                //variables
                List<int> listMaleFemaleCount = new List<int>();
                int i = 0;
                MaleFemale MaleFemaleObj = new MaleFemale();


                //call BAL method to get Male and Female mindtree minds count 
                listMaleFemaleCount = await this._dashBAL.GetMaleFemaleCount();

                //Map into model object
                foreach (var item in listMaleFemaleCount)
                {
                    if (i == GlobalConstants.Constants.INTEGER_ONE)
                        MaleFemaleObj.Male = item;
                    else if (i == GlobalConstants.Constants.INTEGER_TWO)
                        MaleFemaleObj.Female = item;                   
                    ++i;
                }
                return Ok(MaleFemaleObj);
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }
        #endregion
        #region GetEngagementTypeCount
        [HttpGet]
        [ActionName("GetEngagementTypeCount")]
        public async Task<IActionResult> GetEngagementTypeCount()
        {
            try
            {
                //variables
                List<int> listEngagementTypeCount = new List<int>();
                int i = 0;
                EngagementType EngagementTypeObj = new EngagementType();


                //call BAL method to get engagement types and their respective counts 
                listEngagementTypeCount = await this._dashBAL.GetEngagementTypeCount();

                //Map into model object
                foreach (var item in listEngagementTypeCount)
                {
                    if (i == GlobalConstants.Constants.INTEGER_ONE)
                        EngagementTypeObj.notListed = item;
                    else if (i == GlobalConstants.Constants.INTEGER_TWO)
                        EngagementTypeObj.HYB = item;
                    else if (i == GlobalConstants.Constants.INTEGER_THREE)
                        EngagementTypeObj.FPC = item;
                    else if (i == GlobalConstants.Constants.INTEGER_FOUR)
                        EngagementTypeObj.FMC = item;
                    else if (i == GlobalConstants.Constants.INTEGER_FIVE)
                        EngagementTypeObj.TNM = item;
                    ++i;
                }
                return Ok(EngagementTypeObj);
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }
        #endregion
        #region GetDeliveryManager
        [HttpGet]
        [ActionName("GetDeliveryManagerCount")]
        public async Task<IActionResult> GetDeliveryManager()
        {
            try
            {

                //variables
                int i = 0;
                List<DeliveryManagerCount> listDelivery = new List<DeliveryManagerCount>();

                //call BAL method to get Onshore total delivery manager's in mindtree 
                listDelivery = await this._dashBAL.GetDeliveryManager();

                return Ok(listDelivery);

            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
        #region GetVerticalCount
        [HttpGet]
        [ActionName("GetVerticalCount")]
        public async Task<IActionResult> GetVerticalCount()
        {
            try
            {

                //variables
                int i = 0;
                List<VerticalWiseCount> listDelivery = new List<VerticalWiseCount>();

                //call BAL method to get Onshore Offshore mindtree minds count 
                listDelivery = await this._dashBAL.GetVerticalCount();

                return Ok(listDelivery);

            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        #endregion

    }
}