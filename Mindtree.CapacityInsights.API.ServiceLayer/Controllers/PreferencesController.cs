﻿/***************************************************************************************************************************
* Page Name  : PreferencesController.cs
* Created By : Adabala Venkat Sujit 
* Created On : 14 Nov 2018
* Description : Preference related API's are coded in this controller
* Modified By  : Supraja Harish   
* Modified Date : 24 Jan 2018   
* Modified Reason: 
***************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Newtonsoft.Json;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        #region private variables
        /// <summary>
        /// object to instantiate the preference BAL class
        /// </summary>
        private readonly IPreferenceBAL _prefBAL;

        /// <summary>
        /// object to instantiate the capacity BAL class
        /// </summary>
        private readonly ICapacityBAL _capacityBAL;
        #endregion

        #region Constructor
        public PreferencesController(IPreferenceBAL prefBAL,ICapacityBAL capBAL)
        {
            this._prefBAL = prefBAL;
            this._capacityBAL=capBAL;
        }
        #endregion

        #region SavePreference
        //This API is used in abcd.mindtree.com application in Capacity report component.
        /// <summary>
        /// this is controller post method to save preferences
        /// </summary>
        /// <param name="preferencesModelDTO">Request body contains object of type PreferenceModelDTO</param>
        /// <returns>Response code</returns>
        [HttpPost("SavePreference")]
        public async Task<IActionResult> SavePreference([FromBody]PreferencesModelDTO preferencesModelDTO)
        {
            try
            {
                await _prefBAL.SavePreferences(preferencesModelDTO);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }            
            
        }
        #endregion

        #region GetProfilePreference 
        //This API is used in abcd.mindtree.com application in preference component.
        /// <summary>
        /// Get all the preferences saved by the current user
        /// </summary>
        /// <param name="MID">MID of the logged in user</param>
        /// <returns>list of all prefernces saved by the current user</returns>
        [HttpGet("GetProfilePreference")]
        public async Task<IActionResult> GetProfilePreference(string MID)
        {
            try
            {

                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    List<PreferencesModelDTO> res =await _prefBAL.GetTblProfilePreference(MID);
                    return Ok(res);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                return NotFound();
            } 
        }
        #endregion

        #region GetDefaultPreference 
        //This API is used in abcd.mindtree.com application in Capacity report component.
        /// <summary>
        /// To get default preference if any for the current user
        /// </summary>
        /// <param name="MID">MID of the current user</param>
        /// <returns>Defalut preference of the current user</returns>
        [HttpGet("GetDefaultPreference")]
        public async Task<IActionResult> GetDefaultPreference(string MID)
        {
            try
            {
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                  //  List<PreferencesModelDTO> res = _prefBAL.GetTblProfilePreference(ID);
                    
                    return Ok(await _prefBAL.GetDefaultPreference(MID));
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

                 return StatusCode(500);
            }
        }
        #endregion

        #region GetDefaultPreferenceForDisplay
        //This API is used in abcd.mindtree.com application in preference component.
        /// <summary>
        /// To get default preference for display
        /// </summary>
        /// <param name="MID">MID of the current user</param>
        /// <returns>Default prefernce DTO</returns>
        [HttpGet("GetDefaultPreferenceForDisplay")]
        public async Task<IActionResult> GetDefaultPreferenceForDisplay(string MID)
        {
            try
            {
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    //  List<PreferencesModelDTO> res = _prefBAL.GetTblProfilePreference(ID);

                    return Ok(await _prefBAL.GetDefaultPreferenceForDisplay(MID));
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region ChangeDefaultPreference
        //This API is used in abcd.mindtree.com application in preference component.
        /// <summary>
        /// This method is used to change the  default preference
        /// </summary>
        /// <param name="preference">New prefernce object to be updated as default</param>
        /// <returns>action code</returns>
        [HttpPost("ChangeDefaultPreference")]
        public async  Task<IActionResult> ChangeDefaultPreference(PreferencesModelDTO preference)
        {
            try
            {
               await this._prefBAL.ChangeDefaultPreference(preference);
                return Ok();
            }
            catch (Exception ex)
            {

                return  StatusCode(500); 
            }
           
        }
        #endregion
    }
}