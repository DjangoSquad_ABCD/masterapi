﻿/************************************************************************************************
* Page Name  : RoleController.cs
* Created By : Hanisha Hanumula 
* Created On : 5 Nov 2018
* Description : role controller
* Modified By  :   
* Modified Date :   
* Modified Reason:  
*************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers;
using Mindtree.CapacityInsights.API.DataLayer.DTO;
using Mindtree.CapacityInsights.API.Entities.DataEntities;
using Mindtree.CapacityInsights.API.Utilities;
using Newtonsoft.Json;

namespace Mindtree.CapacityInsights.API.ServiceLayer.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class RoleController : Controller
    {

        #region private variables 
        private readonly IRoleBAL _rolBAL;
        private readonly ICapacityBAL _capacityBAL;
        public IConfiguration Configuration { get; }
        private readonly IDataEncryptionDecryption _dataEncryptionDecryption;
        #endregion

        #region constructor 
        public RoleController(IRoleBAL rolBAL, ICapacityBAL capacityBAL, IConfiguration Configuration, IDataEncryptionDecryption dataEncryptionDecryption)
        {
            this._rolBAL = rolBAL;
            this._capacityBAL = capacityBAL;
            this.Configuration = Configuration;
            this._dataEncryptionDecryption = dataEncryptionDecryption;
        }
        #endregion

        #region Postrole
        // this api is used in abcd.mindtree.com in admin component
        /// <summary>
        /// this is controller post method to save roles
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Postrole(RoleModelDTO role)
        {
            try
            {
                int roleJsonobj = Convert.ToInt16(JsonConvert.SerializeObject(role));
                await _rolBAL.CreateRole(role);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }
        #endregion

        #region GetRole
        // this api is used in abcd.mindtree.com in admin component
        /// <summary>
        /// this is controller get method to view roles by MID
        /// </summary>
        /// <param name="MID"></param>
        /// <returns></returns>


        [HttpGet]
        [ActionName("GetUserRole")]
        public async Task<ActionResult<int>> GetRole(string MID)
        {
            try
            {    
                //Regular expression for valid mindtree ID that does not have M included in it.
                Regex startsWithDigit = new Regex("^\\d{7}");

                //Regular expression for valid mindtree ID starting with M.
                Regex startsWithM = new Regex("^[M]{1}\\d{7}");

                //check if either of the expressions are valid
                bool validIDFormt = startsWithDigit.IsMatch(MID) || startsWithM.IsMatch(MID);

                //proceed further only if the ID is in the valid format
                if (validIDFormt)
                {
                    int role = await this._rolBAL.GetRole(MID);
                    return role;
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }


        }
        #endregion

        #region GetAllRoles
        // this api is used in abcd.mindtree.com in admin component
        /// <summary>
        /// this is controller get method to view all roles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetAllAdmins")]
        public async Task<IActionResult>  GetAllRoles()
        {   
            try
            {
                // return all admins data
                return Json(await this._rolBAL.GetAllAdmins());
            }
            catch(Exception ex)
            {
                return NotFound();
            }
           
        }
        #endregion

        #region CreateRole
        // this api is used in abcd.mindtree.com in admin component
        /// <summary>
        /// this is controller get method to create role
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("CreateRole")]
        public async Task<ActionResult> CreateRole([FromBody]RoleModelDTO role)
        {
            try
            {

                string MidOfNewAdmin ="" , MidOfUser = "", RoleOfUser= "";
                byte[] encryptMIDOfNewAdmin = Convert.FromBase64String(role.MID);
                byte[] encryptMIDOfLoggedInUser = Convert.FromBase64String(role.MidOfLoggedInUser);
                byte[] encryptRoleOfAdmin = Convert.FromBase64String(role.Role);
               // var encryptedMID = System.Text.Encoding.UTF8.GetString(encryptMIDOfNewAdmin);
                MidOfNewAdmin = await _dataEncryptionDecryption.Decryption(encryptMIDOfNewAdmin);
                MidOfUser = await _dataEncryptionDecryption.Decryption(encryptMIDOfLoggedInUser);
                RoleOfUser = await _dataEncryptionDecryption.Decryption(encryptRoleOfAdmin);
               
                RoleModelDTO decryptedRole = new RoleModelDTO
                {
                    MID = MidOfNewAdmin,
                    MidOfLoggedInUser = MidOfUser,
                    Role = RoleOfUser
                };
               
                bool IsAdmin = await _rolBAL.VerifyAdminRole(decryptedRole.MidOfLoggedInUser);
                if (IsAdmin)
                {
                    // replacing MID starting with m to M 
                    if (decryptedRole.MID.StartsWith('m'))
                    {
                        decryptedRole.MID = role.MID.Replace('m', 'M');
                    }
                    //Check if user ID is present in the capacity table, if yes then proceed further.
                //    RoleModelDTO decryptedRole = new RoleModelDTO()
                //    {
                //        MID = MidOfNewAdmin;
                //    MidOfLoggedInUser = MidOfUser;

                //}
                if (await _capacityBAL.IsValidEmployee(decryptedRole.MID))
                {
                    var rolesJsonobj = JsonConvert.SerializeObject(decryptedRole);
                    var response = await this._rolBAL.CreateRole(decryptedRole);
                    if (response)
                    {
                        return Ok();
                    }
                    return BadRequest();
                }
                else
                {
                    return StatusCode(501);
                }

            }
                else
                {
                return StatusCode(401);
            }
        }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion

        #region DeleteRole
        // this api is used in abcd.mindtree.com in admin component
        /// <summary>
        /// this is controller get method to delete role
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("DeleteAdmin")]
        public async Task<ActionResult> DeleteAdmin([FromBody]UserModelDTO role)
        {
            try
            {
                if (!role.EmployeeID.StartsWith('M'))
                {
                    role.EmployeeID = 'M' + role.EmployeeID;
                }

                    var rolesJsonobj = JsonConvert.SerializeObject(role);
                    var response = await this._rolBAL.DeleteRole(role);
                    if (response)
                    {
                        return Ok();
                    }
                    return BadRequest();
                

            }

            catch (Exception ex)
            {
                return NotFound();
            }
        }
        #endregion
    }
}
