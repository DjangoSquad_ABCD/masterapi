﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Mindtree.CapacityInsights.API.BusinessLayer.Interfaces;
using Mindtree.CapacityInsights.API.BusinessLayer.ServiceManagers;
using Mindtree.CapacityInsights.API.DataLayer;
using Mindtree.CapacityInsights.API.DataLayer.Interfaces;
using Mindtree.CapacityInsights.API.DataLayer.Repositories;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Http;
using Mindtree.CapacityInsights.API.Utilities;

namespace Mindtree.CapacityInsights.API.ServiceLayer
{
    public class Startup
    {
        // variable generated for calling IConfiguration methods
        public IConfiguration Configuration { get; }

        // variable generated for storing the connection string
        public static string ConnectionString

        {
            get;
            private set;
        }

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appSettings.json").Build();
            if (env.IsDevelopment())
            {
                // it is the connection string used in development phase or local
                ConnectionString = Configuration["sqlserver:ConnectionStringDev"];
            }
            else
            {
                //it is the connection string used in production phase
                ConnectionString = Configuration["sqlserver:ConnectionStringProd"];
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CapacityInsightsDBContext>(options => options.UseSqlServer(ConnectionString));
            services.AddCors(options =>
            {   
                // CORS policy is added so that the data can be communicated from API to UI or vice-versa or to allow cross platform communication
                options.AddPolicy("CorsPolicy",
                builder => builder.AllowAnyOrigin()
                                    .AllowAnyMethod()
                                    .AllowAnyHeader()
                                    .AllowCredentials());
            });
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                    {
                        //options.RequireHttpsMetadata = false;
                        //options.SaveToken = false;
                        //options.Audience = "Ozone";
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = "Ozone",
                            ValidAudience = "Ozone",
                            IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),  // Signing Key for encoding the JWT token.
                            TokenDecryptionKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["Jwt:SecretKey"]))  //  Key for decrypting the token 
                        };
                    });

            //configuring Ihttp client to make http requests
            services.AddHttpClient();
            services.AddScoped<IPreferenceBAL, PreferenceBAL>();
            services.AddScoped<IPreferenceRepository, PreferenceRepository>();
            services.AddScoped<IRoleBAL, RoleBAL>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ICapacityBAL, CapacityBAL>();
            services.AddScoped<ICapacityRepository, CapacityRepository>();
            services.AddScoped<IFilterBAL, FilterBAL>();
            services.AddScoped<IFilterRepository, FilterRepository>();
            services.AddScoped<IDashBoardBAL, DashBoardBAL>();
            services.AddScoped<IDashBoardRepository, DashBoardRepository>();
            services.AddScoped<IAuditLogBAL, AuditLogBAL>();
            services.AddScoped<IAuditLogRepository, AuditLogRepository>();
            services.AddScoped<IThemeBAL, ThemeBAL>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<IDataEncryptionDecryption, DataEncryptionDecryption>();
            services
               .AddMvcCore(options =>
               {
                   options.RequireHttpsPermanent = true; // does not affect api requests
                   options.RespectBrowserAcceptHeader = true; // false by default
                                                      
               });

            // For adding HSTS headers in all calls
            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(60);
                //options.ExcludedHosts.Add("https://localhost:44323/api/values");
                //options.ExcludedHosts.Add("www.example.com");
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)

            {
                //To deny the X-frame and disable clickjacking
                app.Use(async (context, next) =>
                {
                    context.Response.Headers.Add("X-Frame-Options", "DENY");
                    await next();
                });

                 // HSTS heder will redirect to https even if http request is raised
                var options = new RewriteOptions()
                .AddRedirectToHttps(StatusCodes.Status301MovedPermanently, 63423);
                app.UseRewriter(options);
                app.UseHsts();
                app.UseCors("CorsPolicy");
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                else
                {

                    app.UseHsts();
                }
                app.UseHttpsRedirection();
                app.UseAuthentication();
                app.UseMvc();
            }
      }
}
