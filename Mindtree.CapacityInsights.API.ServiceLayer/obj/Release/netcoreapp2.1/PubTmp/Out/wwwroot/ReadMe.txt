﻿->Steps we followed while creating the project
   i)create new project 
  ii)choose Asp.net core web application 
 iii)Choose web api with .net core version 2.1

->ProjectName:Mindtree.CapacityInsightsAPI

->ProjectDescreption:* will be updated soon*

->ProjectURL:https://abcdapi.azurewebsites.net

->Project Technology Stack:*Will be updated soon*

->Project Layers:
   i)ServiceLayer.
  ii)BusinessLayer.
 iii)DataLayer
  iv)Entities

->Project Flow:
•	This project serves all the appropriate http requests from front end application. 
•	ServiceLayer receives the input request, Serves it by interacting with businesslayer.
•	BusinessLayer holds all the business logic, Interacts with DataLayer to respond to ServiceLayer request. 
•	DataLayer use entity classes in entity layer and CapacityInsightsDBContext.cs class in this layer to perform any of the 
    CRUD operations on the database.

->Project ServiceLayer is build on: .Net core 2.1 

->Project Database: CapacityInsightsDB

->AzureDB ConnectionString:*will be updated soon*

->ORM(object relationship management) tool used:EntityFrameWork core
   Nuget packages required for ef core
  i)Microsoft.EntityFrameworkCore
  ii)Microsoft.EntityFrameworkCore.Tools
  iii)Microsoft.EntityFrameworkCore.SqlServer

->Command to scaffold entity classes from DB:Scaffold-DbContext "Server=.;Database=CapacityInsightsDB;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir DataEntities

->For ef core basics please refer the below link:
  https://docs.microsoft.com/en-us/ef/#pivot=efcore

->For .Net core basics please follow the below link:
  https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-2.1

->Migrations Steps to be followed while updating db as we are following code fist approach
Run the below commands in package manager console for the project where db context class is present 
  Add-Migration Name_of_migration
  Update-Database
