﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindtree.CapacityInsights.API.Utilities
{
    public class GlobalConstants
    {
        public struct Constants
        {
            public const string USER_ACTION_LOGGED_IN = "LoggedIn";
            public const string MY_INTEREST_CLICKED = "MyInterestClicked";
            public const int INTEGER_ONE = 1;
            public const int INTEGER_TWO = 2;
            public const int INTEGER_THREE = 3;
            public const int INTEGER_FOUR = 4;
            public const int INTEGER_FIVE = 5;
            public const string AESSECRETKEY = "dsqwertuujexn4t34veem8ijtffd4vyn";
            public const string AESIV = "45dm3pufjr2fdmwk";

        }
    }
}
