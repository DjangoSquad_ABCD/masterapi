﻿/************************************************************************************************
* Page Name  : IDataEncryptionDecryption.cs
* Created By : Ashi Gupta
* Created On : 2 Nov 2018
* Description : Interface containing methods for AES encryption and decryption
* Modified By  :   
* Modified Date :   
* Modified Reason:  
*************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.Utilities
{
    public interface IDataEncryptionDecryption
    {
        Task<string> Decryption(byte[] data);
        Task<string> Encryption(string data);


    }
}
