﻿/************************************************************************************************
* Page Name  : DataEncryptionDecryption.cs
* Created By : Ashi Gupta
* Created On : 2 Nov 2018
* Description : Class containing methods for AES encryption and decryption
* Modified By  :   
* Modified Date :   
* Modified Reason:  
*************************************************************************************************/
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Mindtree.CapacityInsights.API.Utilities
{
    public class DataEncryptionDecryption:IDataEncryptionDecryption
    {
        public DataEncryptionDecryption()
        { }

        #region Decryption
        public async Task<string> Decryption(byte[] data)
        {
            try
            {
                
                string plainText, MID, encryptedRole;
                using (Aes encryptor = Aes.Create())
                {
                    var key = Encoding.UTF8.GetBytes(GlobalConstants.Constants.AESSECRETKEY);
                    var iv = Encoding.UTF8.GetBytes(GlobalConstants.Constants.AESIV);
                    //encryptText = EncryptedMID;
                    

                    //var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                    using (var rijAlg = new RijndaelManaged())
                    {
                        //Settings  
                        rijAlg.Mode = CipherMode.CBC;
                        rijAlg.Padding = PaddingMode.PKCS7;
                        rijAlg.FeedbackSize = 128;

                        rijAlg.Key = key;
                        rijAlg.IV = iv;

                        // Create a decrytor to perform the stream transform.  
                        var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                        try
                        {
                            // Create the streams used for decryption.  
                            using (var msDecrypt = new MemoryStream(data))
                            {
                                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                                {

                                    using (var srDecrypt = new StreamReader(csDecrypt))
                                    {
                                        // Read the decrypted bytes from the decrypting stream  
                                        // and place them in a string.  
                                        plainText = srDecrypt.ReadToEnd();

                                    }

                                }
                            }
                        }
                        catch
                        {
                            plainText = "keyError";
                        }
                    }
                }
                return plainText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Encryption
        public async Task<string>Encryption(string data)
        {
            try
            {
                string encryptedDecodedData = "";
                using (Aes encryptor = Aes.Create())
                {
                    var key = Encoding.UTF8.GetBytes(GlobalConstants.Constants.AESSECRETKEY);
                    var iv = Encoding.UTF8.GetBytes(GlobalConstants.Constants.AESIV);
                    byte[] encrypted;
                    using (var rijAlg = new RijndaelManaged())
                    {
                        rijAlg.Mode = CipherMode.CBC;
                        rijAlg.Padding = PaddingMode.PKCS7;
                        rijAlg.FeedbackSize = 128;
                        rijAlg.Key = key;
                        rijAlg.IV = iv;
                        // Create a decrytor to perform the stream transform.  
                        var encrypt = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                        // Create the streams used for encryption.  
                        using (var msEncrypt = new MemoryStream())
                        {
                            using (var csEncrypt = new CryptoStream(msEncrypt, encrypt, CryptoStreamMode.Write))
                            {
                                //encrypted = msEncrypt.ToArray();
                                using (var swEncrypt = new StreamWriter(csEncrypt))
                                {
                                    //Write all data to the stream.  
                                    swEncrypt.Write(data);
                                }
                                encrypted = msEncrypt.ToArray();
                                encryptedDecodedData = Convert.ToBase64String(encrypted);
                            }
                        }
                    }
                }
                return encryptedDecodedData;
            
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        #endregion
    }

}

