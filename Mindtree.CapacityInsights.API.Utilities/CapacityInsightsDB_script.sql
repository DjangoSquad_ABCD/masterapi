USE [master]
GO
/****** Object:  Database [CapacityInsightsDB]    Script Date: 11/6/2018 2:28:52 PM ******/
CREATE DATABASE [CapacityInsightsDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CapacityInsightsDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CapacityInsightsDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CapacityInsightsDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CapacityInsightsDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CapacityInsightsDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CapacityInsightsDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CapacityInsightsDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CapacityInsightsDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CapacityInsightsDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CapacityInsightsDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CapacityInsightsDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CapacityInsightsDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET RECOVERY FULL 
GO
ALTER DATABASE [CapacityInsightsDB] SET  MULTI_USER 
GO
ALTER DATABASE [CapacityInsightsDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CapacityInsightsDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CapacityInsightsDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CapacityInsightsDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CapacityInsightsDB]
GO
/****** Object:  Table [dbo].[tblAuditLog]    Script Date: 11/6/2018 2:28:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblAuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [varchar](50) NULL,
	[EmployeeName] [varchar](100) NULL,
	[UserAction] [varchar](50) NULL,
	[ReqID] [varchar](50) NULL,
	[Reference] [varchar](max) NULL,
	[LastUpdatedOn] [datetime] NULL,
	[UploadedDateTime] [datetime] NULL,
	[UploadedBy] [nvarchar](32) NULL,
	[UploadedFor] [nvarchar](32) NULL,
 CONSTRAINT [PK_tblAuditLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCapacityReport]    Script Date: 11/6/2018 2:28:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCapacityReport](
	[EmployeeID] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Competency] [nvarchar](max) NULL,
	[GivenRole] [nvarchar](max) NULL,
	[Segment] [nvarchar](max) NULL,
	[Location] [nvarchar](max) NULL,
	[OnOffShore] [nvarchar](max) NULL,
	[EmpStatus] [nvarchar](max) NULL,
	[Billability] [nvarchar](max) NULL,
	[Vertical] [nvarchar](max) NULL,
	[SubVertical] [nvarchar](max) NULL,
	[Practice] [nvarchar](max) NULL,
	[SubPractice] [nvarchar](max) NULL,
	[CustomerName] [nvarchar](max) NULL,
	[ProfitCenter] [nvarchar](max) NULL,
	[ProjectID] [nvarchar](max) NULL,
	[ProjectName] [nvarchar](max) NULL,
	[EngagementType] [nvarchar](max) NULL,
	[ProjectType] [nvarchar](max) NULL,
	[ProjectRole] [nvarchar](max) NULL,
	[ProjectManagerRequestor] [nvarchar](max) NULL,
	[DeliveryManager] [nvarchar](max) NULL,
	[DeliveryPartner] [nvarchar](max) NULL,
	[IGDeliveryHead] [nvarchar](max) NULL,
	[BillableStatus] [nvarchar](max) NULL,
	[StartDate] [nvarchar](max) NULL,
	[EndDate] [nvarchar](max) NULL,
	[SRStatus] [nvarchar](max) NULL,
	[StaffingRequestID] [nvarchar](max) NULL,
	[ReasonForBlocking] [nvarchar](max) NULL,
	[ReasonForUnbillable] [nvarchar](max) NULL,
	[UnbillableDays] [nvarchar](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[CampusTag] [nvarchar](max) NULL,
	[BillablePercentageStaffed] [float] NULL,
	[UnbillablePercentageStaffed] [float] NULL,
	[TotalPercentageStaffed] [float] NULL,
	[Technology] [nvarchar](max) NULL,
	[DateOfJoining] [nvarchar](max) NULL,
	[GivenFunction] [nvarchar](max) NULL,
	[AccountGroup] [nvarchar](max) NULL,
	[VisaCategory] [nvarchar](max) NULL,
	[SubCategory] [nvarchar](max) NULL,
	[GivenType] [nvarchar](max) NULL,
	[ExpiryDate] [nvarchar](max) NULL,
	[StampingStatus] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[RequiredYear] [nvarchar](max) NULL,
	[IJPStuatus] [nvarchar](max) NULL,
	[IJPAppliedSRID] [nvarchar](max) NULL,
	[IJPAppliedDate] [nvarchar](max) NULL,
	[WorkLocation] [nvarchar](max) NULL,
	[KLocation] [nvarchar](max) NULL,
	[CustomerGroup] [nvarchar](max) NULL,
	[BilledF] [float] NULL,
	[UnbilledF] [int] NULL,
	[BlockedF] [int] NULL,
	[InternalPrjInvestmentF] [int] NULL,
	[InternalPrjCostF] [int] NULL,
	[AvailableF] [int] NULL,
	[NonDeployF] [int] NULL,
	[TrainingF] [int] NULL,
	[LongLeaveF] [int] NULL,
	[SalesF] [int] NULL,
	[SupportF] [int] NULL,
	[PeopleOwner] [nvarchar](max) NULL,
	[AccountCategory] [nvarchar](max) NULL,
	[PrimaryL1] [nvarchar](max) NULL,
	[PrimaryL2] [nvarchar](max) NULL,
	[PrimaryL3] [nvarchar](max) NULL,
	[PrimaryL4] [nvarchar](max) NULL,
	[SecondaryL1] [nvarchar](max) NULL,
	[SecondaryL2] [nvarchar](max) NULL,
	[SecondaryL3] [nvarchar](max) NULL,
	[SecondaryL4] [nvarchar](max) NULL,
	[TertiaryL1] [nvarchar](max) NULL,
	[TertiaryL2] [nvarchar](max) NULL,
	[TertiaryL3] [nvarchar](max) NULL,
	[TertiaryL4] [nvarchar](max) NULL,
	[RTPF] [nvarchar](max) NULL,
	[Resigned] [nvarchar](max) NULL,
	[CurrentWorkLocation] [nvarchar](max) NULL,
	[PreferredWorkLocation] [nvarchar](max) NULL,
	[LastStaffingEndDate] [nvarchar](max) NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdatedOn] [varchar](50) NULL,
	[IsDataCorrect] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[tblCapacityReport] ADD [ConfirmedOn] [varchar](50) NULL
ALTER TABLE [dbo].[tblCapacityReport] ADD [Yorbit101] [nvarchar](max) NULL
ALTER TABLE [dbo].[tblCapacityReport] ADD [Yorbit201] [nvarchar](max) NULL
ALTER TABLE [dbo].[tblCapacityReport] ADD [Yorbit301] [nvarchar](max) NULL
ALTER TABLE [dbo].[tblCapacityReport] ADD [TotalBlockedDays] [nvarchar](max) NULL
ALTER TABLE [dbo].[tblCapacityReport] ADD [TotalExperience] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblExceptionLog]    Script Date: 11/6/2018 2:28:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblExceptionLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExceptionLevel] [varchar](10) NULL,
	[Source] [varchar](max) NULL,
	[Message] [varchar](max) NULL,
	[StackTrace] [text] NULL,
	[TargetSite] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_tblExceptionLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblProfilePrefrence]    Script Date: 11/6/2018 2:28:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProfilePrefrence](
	[MID] [nchar](10) NOT NULL,
	[Filters] [nvarchar](max) NULL,
 CONSTRAINT [PK_tblProfilePrefrence] PRIMARY KEY CLUSTERED 
(
	[MID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[tblCapacityReport] ADD  CONSTRAINT [DF_tblCapacityReport_IsDataCorrect]  DEFAULT ((1)) FOR [IsDataCorrect]
GO
USE [master]
GO
ALTER DATABASE [CapacityInsightsDB] SET  READ_WRITE 
GO
